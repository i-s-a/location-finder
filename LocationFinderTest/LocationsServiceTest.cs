using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Linq;

using LocationFinder;
using LocationFinder.Repositories;
using LocationFinder.Services;
using LocationFinder.Requests;
using LocationFinder.Models;
using GeoMathLib;

namespace LocationFinderTest
{
    [TestClass]
    public class LocationsServiceTest
    {
        private LocationsService _locationsService = null;
        private LocationsContext _locationsContext = null;
        private LocationsService _sampleDataLocationsService = null;
        private LocationsContext _sampleDataLocationsContext = null;

        private static readonly string TEST_DATA_FILENAME = @"..\..\..\TestFiles\TestData.txt";
        private static readonly string TEST_COUNTRYCODE_FILENAME = @"..\..\..\TestFiles\CountryCodes.csv";
        private static readonly string TEST_FEATURECODE_FILENAME = @"..\..\..\TestFiles\featureCodes_en.txt";

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            var options = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=LocationFinderTest.db")
                .Options;

            var locationsContext = new LocationsContext(options);

            bool created = locationsContext.Database.EnsureCreated();
            if (created)
            {
                PopulateCountryCodes(locationsContext).GetAwaiter().GetResult();
                PopulateFeatureCodes(locationsContext).GetAwaiter().GetResult();
            }

            var sampleDataOptions = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=SampleDataLocationFinderTest.db")
                .Options;

            var sampleDataLocationsContext = new LocationsContext(sampleDataOptions);

            ILocationsRepository<long> sampleDataLocationsRepository = 
                new SqliteLocationsRepository(sampleDataLocationsContext);
            var sampleDataLocationsService = new LocationsService(sampleDataLocationsRepository);

            created = sampleDataLocationsContext.Database.EnsureCreated();
            if (created)
            {
                PopulateCountryCodes(sampleDataLocationsContext, TEST_COUNTRYCODE_FILENAME).GetAwaiter().GetResult();
                PopulateFeatureCodes(sampleDataLocationsContext, TEST_FEATURECODE_FILENAME).GetAwaiter().GetResult();

                //Populate with some seed data
                PopulateSeedData(sampleDataLocationsContext, TEST_DATA_FILENAME).GetAwaiter().GetResult();
            }
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            var options = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=LocationFinderTest.db")
                .Options;

            var locationsContext = new LocationsContext(options);
            locationsContext.Database.EnsureDeleted();

            var sampleDataOptions = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=SampleDataLocationFinderTest.db")
                .Options;

            var sampleDataLocationsContext = new LocationsContext(sampleDataOptions);

            sampleDataLocationsContext.Database.EnsureDeleted();
        }

        [TestInitialize]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=LocationFinderTest.db")
                .Options;

            _locationsContext = new LocationsContext(options);

            ILocationsRepository<long> locationsRepository = new SqliteLocationsRepository(_locationsContext);
            _locationsService = new LocationsService(locationsRepository);

            bool created = _locationsContext.Database.EnsureCreated();


            var sampleDataOptions = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite("Data Source=SampleDataLocationFinderTest.db")
                .Options;

            _sampleDataLocationsContext = new LocationsContext(sampleDataOptions);

            ILocationsRepository<long> sampleDataLocationsRepository =
                new SqliteLocationsRepository(_sampleDataLocationsContext);

            _sampleDataLocationsService = new LocationsService(sampleDataLocationsRepository);
        }

        [TestMethod]
        public async Task AddLocationAutoId_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);
        }

        [TestMethod]
        public async Task AddLocationAutoId_NoName_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            }));
        }

        [TestMethod]
        public async Task AddLocationAutoId_NoCountryCodeId_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = null,
                FeatureCodeId = "PPL",
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task AddLocationAutoId_NoFeatureCodeId_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = null,
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task AddLocationAutoId_InvalidCoordinates_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 91.0, Longitude = -0.13947 }
            }));
        }

        [TestMethod]
        public async Task AddLocationWithId_Test()
        {
            var response = await _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            });

            Assert.IsTrue(response.Id == 10_000);
            Assert.IsTrue(response.Name == "Hove");
            Assert.IsTrue(response.Coordinates.Latitude == 50.83088);
            Assert.IsTrue(response.Coordinates.Longitude == -0.1672);
        }

        [TestMethod]
        public async Task AddLocationWithId_NoName_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            }));
        }

        [TestMethod]
        public async Task AddLocationWithId_NoCountryCodeId_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = null,
                FeatureCodeId = "PPL",
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task AddLocationWithId_NoFeatureCodeId_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = null,
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task AddLocationWithId_NoCoordinates_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task AddLocationWithId_InvalidCoordinates_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -181.0 }
            }));
        }

        [TestMethod]
        public async Task LocationFullUpdate_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.CreateOrUpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "EG",
                FeatureCodeId = "AIRP",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Hove");
            Assert.IsTrue(updateResponse.CountryCodeId == "EG");
            Assert.IsTrue(updateResponse.FeatureCodeId == "AIRP");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.83088);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.1672);
        }

        [TestMethod]
        public async Task LocationFullUpdate_NoName_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            })); 
        }

        [TestMethod]
        public async Task LocationFullUpdate_NoCountryCodeId_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = null,
                FeatureCodeId = "PPL",
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task LocationFullUpdate_NoFeatureCodeId_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(10_000, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = null,
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task LocationFullUpdate_NoCoordinates_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = null
            }));
        }

        [TestMethod]
        public async Task LocationFullUpdate_InvalidCoordinates_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => _locationsService.CreateOrUpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -181.0 }
            }));
        }

        [TestMethod]
        public async Task LocationPartialUpdate_All_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.UpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "Hove",
                CountryCodeId = "EG",
                FeatureCodeId = "AIRP",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Hove");
            Assert.IsTrue(updateResponse.CountryCodeId == "EG");
            Assert.IsTrue(updateResponse.FeatureCodeId == "AIRP");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.83088);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.1672);
        }

        [TestMethod]
        public async Task LocationPartialUpdate_Name_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.UpdateLocation(response.Id, new UpdateLocationRequest
            {
                Name = "Hove",
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Hove");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.13947);
        }

        [TestMethod]
        public async Task LocationPartialUpdate_CountryCodeId_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.UpdateLocation(response.Id, new UpdateLocationRequest
            {
                CountryCodeId = "EG",
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Brighton");
            Assert.IsTrue(updateResponse.CountryCodeId == "EG");
            Assert.IsTrue(updateResponse.FeatureCodeId == "PPL");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.13947);
        }

        [TestMethod]
        public async Task LocationPartialUpdate_FeatureCodeId_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.UpdateLocation(response.Id, new UpdateLocationRequest
            {
                FeatureCodeId = "AIRP",
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Brighton");
            Assert.IsTrue(updateResponse.CountryCodeId == "GB");
            Assert.IsTrue(updateResponse.FeatureCodeId == "AIRP");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.13947);
        }

        [TestMethod]
        public async Task LocationPartialUpdate_Coordinates_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            var updateResponse = await _locationsService.UpdateLocation(response.Id, new UpdateLocationRequest
            {
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            });

            Assert.IsTrue(updateResponse.Id == response.Id);
            Assert.IsTrue(updateResponse.Name == "Brighton");
            Assert.IsTrue(updateResponse.CountryCodeId == "GB");
            Assert.IsTrue(updateResponse.FeatureCodeId == "PPL");
            Assert.IsTrue(updateResponse.Coordinates.Latitude == 50.83088);
            Assert.IsTrue(updateResponse.Coordinates.Longitude == -0.1672);
        }

        [TestMethod]
        public async Task LocationPartialUpdate_NotFound_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => _locationsService.UpdateLocation(response.Id + 1_000_000_000, new UpdateLocationRequest
            {
                Name = "Hove",
                Coordinates = new GeoCoordinates { Latitude = 50.83088, Longitude = -0.1672 }
            }));
        }

        [TestMethod]
        public async Task DeleteLocation_Test()
        {
            var response = await _locationsService.CreateLocation(new NewLocationRequest
            {
                Name = "Brighton",
                CountryCodeId = "GB",
                FeatureCodeId = "PPL",
                Coordinates = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 }
            });

            Assert.IsTrue(response.Id > 0);
            Assert.IsTrue(response.Name == "Brighton");
            Assert.IsTrue(response.CountryCodeId == "GB");
            Assert.IsTrue(response.FeatureCodeId == "PPL");
            Assert.IsTrue(response.Coordinates.Latitude == 50.82838);
            Assert.IsTrue(response.Coordinates.Longitude == -0.13947);

            await _locationsService.DeleteLocation(response.Id);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => _locationsService.GetLocationById(response.Id));
        }

        [TestMethod]
        public async Task DeleteLocation_NotFound_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => _locationsService.DeleteLocation(10_000_000_000));
        }

        [TestMethod]
        public async Task GetLocationById_Test()
        {
            var response = await _sampleDataLocationsService.GetLocationById(3333133);

            long expectedId = 3333133;
            string expectedName = "Brighton and Hove";
            string expectedCountryCodeId = "GB";
            string expectedFeatureCodeId = "ADM2";
            GeoCoordinates expectedCoordinates = new GeoCoordinates { Latitude = 50.83333, Longitude = -0.13333 };

            Assert.IsNotNull(response);
            Assert.AreEqual(response.Id, expectedId);
            Assert.AreEqual(response.Name, expectedName);
            Assert.AreEqual(response.CountryCodeId, expectedCountryCodeId);
            Assert.AreEqual(response.FeatureCodeId, expectedFeatureCodeId);
            Assert.AreEqual(response.Coordinates, expectedCoordinates);
        }

        [TestMethod]
        public async Task GetLocationById_NotFound_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(
                () => _sampleDataLocationsService.GetLocationById(10_000_000_000));
        }

        [TestMethod]
        public async Task GetLocationByName_Test()
        {
            var responseList = 
                await _sampleDataLocationsService.GetLocationByName("Brighton and Hove", null, null, null, "name");
            var response = responseList.ToList();

            long expectedId = 3333133;
            string expectedName = "Brighton and Hove";
            string expectedCountryCodeId = "GB";
            string expectedFeatureCodeId = "ADM2";
            GeoCoordinates expectedCoordinates = new GeoCoordinates { Latitude = 50.83333, Longitude = -0.13333 };

            Assert.AreEqual(response.Count, 1);
            Assert.AreEqual(response[0].Id, expectedId);
            Assert.AreEqual(response[0].Name, expectedName);
            Assert.AreEqual(response[0].CountryCodeId, expectedCountryCodeId);
            Assert.AreEqual(response[0].FeatureCodeId, expectedFeatureCodeId);
            Assert.AreEqual(response[0].Coordinates, expectedCoordinates);
        }

        [TestMethod]
        public async Task GetLocationByName_NotFound_Test()
        {
            var responseList = 
                await _sampleDataLocationsService.GetLocationByName("RANDOM STRING", null, null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationByName_Count_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByNameLocationsCount("Brighton and Hove", null, false);

            Assert.AreEqual(response, 1);
        }

        [TestMethod]
        public async Task GetLocationByName_Count_NotFound_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByNameLocationsCount("RANDOM STRING", null, false);

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocationByNameAndCountry_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetLocationByName("Brighton and Hove", "GB", null, null, "name");
            var response = responseList.ToList();

            long expectedId = 3333133;
            string expectedName = "Brighton and Hove";
            string expectedCountryCodeId = "GB";
            string expectedFeatureCodeId = "ADM2";
            GeoCoordinates expectedCoordinates = new GeoCoordinates { Latitude = 50.83333, Longitude = -0.13333 };

            Assert.AreEqual(response.Count, 1);
            Assert.AreEqual(response[0].Id, expectedId);
            Assert.AreEqual(response[0].Name, expectedName);
            Assert.AreEqual(response[0].CountryCodeId, expectedCountryCodeId);
            Assert.AreEqual(response[0].FeatureCodeId, expectedFeatureCodeId);
            Assert.AreEqual(response[0].Coordinates, expectedCoordinates);
        }

        [TestMethod]
        public async Task GetLocationByNameAndCountry_NotFound_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetLocationByName("Brighton and Hove", "EG", null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationByNameAndCountry_Count_Test()
        {
            var response =
                await _sampleDataLocationsService.GetByNameLocationsCount("Brighton and Hove", "GB", false);

            Assert.AreEqual(response, 1);
        }

        [TestMethod]
        public async Task GetLocationByNameAndCountry_Count_NotFound_Test()
        {
            var response =
                await _sampleDataLocationsService.GetByNameLocationsCount("Brighton and Hove", "EG", false);

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocationBySimilarName_Asc_Test()
        {
            var responseList = 
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", null, null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 3);

            //By default the list should be returned sorted by Name in acsending order 
            Assert.AreEqual(response[0].Id, 2634959);
            Assert.AreEqual(response[0].Name, "London Victoria station");
            Assert.AreEqual(response[0].CountryCodeId, "GB");
            Assert.AreEqual(response[0].FeatureCodeId, "RSTN");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 51.49571, Longitude = -0.14437 });

            Assert.AreEqual(response[1].Id, 6945127);
            Assert.AreEqual(response[1].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[1].CountryCodeId, "GB");
            Assert.AreEqual(response[1].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 51.50917, Longitude = 0.01798 });

            Assert.AreEqual(response[2].Id, 6695975);
            Assert.AreEqual(response[2].Name, "Victoria Underground Station");
            Assert.AreEqual(response[2].CountryCodeId, "GB");
            Assert.AreEqual(response[2].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[2].Coordinates, new GeoCoordinates { Latitude = 51.49586, Longitude = -0.14354 });
        }

        [TestMethod]
        public async Task GetLocationBySimilarName_Desc_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", null, null, null, "-name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 3);

            Assert.AreEqual(response[0].Id, 6695975);
            Assert.AreEqual(response[0].Name, "Victoria Underground Station");
            Assert.AreEqual(response[0].CountryCodeId, "GB");
            Assert.AreEqual(response[0].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 51.49586, Longitude = -0.14354 });

            Assert.AreEqual(response[1].Id, 6945127);
            Assert.AreEqual(response[1].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[1].CountryCodeId, "GB");
            Assert.AreEqual(response[1].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 51.50917, Longitude = 0.01798 });

            Assert.AreEqual(response[2].Id, 2634959);
            Assert.AreEqual(response[2].Name, "London Victoria station");
            Assert.AreEqual(response[2].CountryCodeId, "GB");
            Assert.AreEqual(response[2].FeatureCodeId, "RSTN");
            Assert.AreEqual(response[2].Coordinates, new GeoCoordinates { Latitude = 51.49571, Longitude = -0.14437 });
        }

        [TestMethod]
        public async Task GetLocationBySimilarName_NotFound_Test()
        {
            var responseList = 
                await _sampleDataLocationsService.GetSimilarLocationsByName("RANDOM STRING", null, null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationBySimilarName_Count_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByNameLocationsCount("Victoria Station", null, true);

            Assert.AreEqual(response, 3);
        }

        [TestMethod]
        public async Task GetLocationBySimilarName_Count_NotFound_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByNameLocationsCount("RANDOM STRING", null, true);

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocationBySimilarNameAndCountry_Asc_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", "GB", null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 3);

            //By default the list should be returned sorted by Name in acsending order 
            Assert.AreEqual(response[0].Id, 2634959);
            Assert.AreEqual(response[0].Name, "London Victoria station");
            Assert.AreEqual(response[0].CountryCodeId, "GB");
            Assert.AreEqual(response[0].FeatureCodeId, "RSTN");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 51.49571, Longitude = -0.14437 });

            Assert.AreEqual(response[1].Id, 6945127);
            Assert.AreEqual(response[1].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[1].CountryCodeId, "GB");
            Assert.AreEqual(response[1].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 51.50917, Longitude = 0.01798 });

            Assert.AreEqual(response[2].Id, 6695975);
            Assert.AreEqual(response[2].Name, "Victoria Underground Station");
            Assert.AreEqual(response[2].CountryCodeId, "GB");
            Assert.AreEqual(response[2].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[2].Coordinates, new GeoCoordinates { Latitude = 51.49586, Longitude = -0.14354 });
        }

        [TestMethod]
        public async Task GetLocationBySimilarNameAndCountry_Desc_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", "GB", null, null, "-name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 3);

            Assert.AreEqual(response[0].Id, 6695975);
            Assert.AreEqual(response[0].Name, "Victoria Underground Station");
            Assert.AreEqual(response[0].CountryCodeId, "GB");
            Assert.AreEqual(response[0].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 51.49586, Longitude = -0.14354 });

            Assert.AreEqual(response[1].Id, 6945127);
            Assert.AreEqual(response[1].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[1].CountryCodeId, "GB");
            Assert.AreEqual(response[1].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 51.50917, Longitude = 0.01798 });

            Assert.AreEqual(response[2].Id, 2634959);
            Assert.AreEqual(response[2].Name, "London Victoria station");
            Assert.AreEqual(response[2].CountryCodeId, "GB");
            Assert.AreEqual(response[2].FeatureCodeId, "RSTN");
            Assert.AreEqual(response[2].Coordinates, new GeoCoordinates { Latitude = 51.49571, Longitude = -0.14437 });
        }

        [TestMethod]
        public async Task GetLocationBySimilarNameAndCountry_Asc_NotFound_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", "EG", null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationBySimilarNameAndCountry_Desc_NotFound_Test()
        {
            var responseList =
                await _sampleDataLocationsService.GetSimilarLocationsByName("Victoria Station", "EG", null, null, "-name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }


        [TestMethod]
        public async Task GetLocationByFeatureCodeId_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByFeatureCodeId("MTRO", null, null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 2);

            //By default the list should be returned sorted by Name in acsending order 
            Assert.AreEqual(response[0].Id, 6945127);
            Assert.AreEqual(response[0].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[0].CountryCodeId, "GB");
            Assert.AreEqual(response[0].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 51.50917, Longitude = 0.01798 });

            Assert.AreEqual(response[1].Id, 6695975);
            Assert.AreEqual(response[1].Name, "Victoria Underground Station");
            Assert.AreEqual(response[1].CountryCodeId, "GB");
            Assert.AreEqual(response[1].FeatureCodeId, "MTRO");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 51.49586, Longitude = -0.14354 });
        }

        [TestMethod]
        public async Task GetLocationByFeatureCodeId_NotFound_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByFeatureCodeId("RANDOM STRING", null, null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationByFeatureCodeId_Count_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByCodeLocationsCount(null, "MTRO");

            Assert.AreEqual(response, 2);
        }

        [TestMethod]
        public async Task GetLocationByFeatureCodeId_Count_NotFound_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByCodeLocationsCount(null, "RANDOM STRING");

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocationByFeatureAndCountryCodeId_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByFeatureCodeId("MUS", "EG", null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 1);

            //By default the list should be returned sorted by Name in acsending order 
            Assert.AreEqual(response[0].Id, 7302952);
            Assert.AreEqual(response[0].Name, "Egyptian Museum");
            Assert.AreEqual(response[0].CountryCodeId, "EG");
            Assert.AreEqual(response[0].FeatureCodeId, "MUS");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 30.04796, Longitude = 31.23368 });
        }

        [TestMethod]
        public async Task GetLocationByFeatureAndCountryCodeId_NotFound_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByFeatureCodeId("MUS", "RANDOM_STRING", null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationByFeatureAndCountryCodeId_Count_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByCodeLocationsCount("GB", "MTRO");

            Assert.AreEqual(response, 2);
        }

        [TestMethod]
        public async Task GetLocationByCountryCodeId_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByCountryCodeId("EG", null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 2);

            //By default the list should be returned sorted by Name in acsending order 
            Assert.AreEqual(response[0].Id, 7302952);
            Assert.AreEqual(response[0].Name, "Egyptian Museum");
            Assert.AreEqual(response[0].CountryCodeId, "EG");
            Assert.AreEqual(response[0].FeatureCodeId, "MUS");
            Assert.AreEqual(response[0].Coordinates, new GeoCoordinates { Latitude = 30.04796, Longitude = 31.23368 });

            Assert.AreEqual(response[1].Id, 355017);
            Assert.AreEqual(response[1].Name, "Pyramids of Giza");
            Assert.AreEqual(response[1].CountryCodeId, "EG");
            Assert.AreEqual(response[1].FeatureCodeId, "PYR");
            Assert.AreEqual(response[1].Coordinates, new GeoCoordinates { Latitude = 29.98333, Longitude = 31.13333 });
        }

        [TestMethod]
        public async Task GetLocationByCountryCodeId_NotFound_Test()
        {
            var responseList = await _sampleDataLocationsService
                .GetLocationsByCountryCodeId("RANDOM STRING", null, null, "name");

            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationByCountryCodeId_Count_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByCodeLocationsCount("EG", null);

            Assert.AreEqual(response, 2);
        }

        [TestMethod]
        public async Task GetLocationByCountryCodeId_Count_NotFound_Test()
        {
            var response = await _sampleDataLocationsService
                .GetByCodeLocationsCount("RANDOM STRING", null);

            Assert.AreEqual(response, 0);
        }


        [TestMethod]
        public async Task GetLocationsWithInBoundary_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 50.86155, Longitude = -0.08361 };
            double distanceKm = 1;
            var responseList = 
                await _sampleDataLocationsService.GetLocationsWithinBoundary(location, distanceKm, null, null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 3);

            //By default the list should be returned sorted by Name in acsending order
            Assert.AreEqual(response[0].Id, 9256358);
            Assert.AreEqual(response[0].Name, "American Express Community Stadium");

            Assert.AreEqual(response[1].Id, 2649716);
            Assert.AreEqual(response[1].Name, "Falmer");

            Assert.AreEqual(response[2].Id, 6945667);
            Assert.AreEqual(response[2].Name, "Falmer Railway Station");
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundary_NotFound_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 30.033333, Longitude = 31.233334 };
            double distanceKm = 1;
            var responseList =
                await _sampleDataLocationsService.GetLocationsWithinBoundary(location, distanceKm, null, null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundary_Count_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 50.86155, Longitude = -0.08361 };
            double distanceKm = 1;
            var response = await _sampleDataLocationsService.GetLocationsWithinBoundaryCount(location, distanceKm, null);

            Assert.AreEqual(response, 3);
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundary_Count_NotFound_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 30.033333, Longitude = 31.233334 };
            double distanceKm = 1;
            var response = await _sampleDataLocationsService.GetLocationsWithinBoundaryCount(location, distanceKm, null);

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundaryAndFeature_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 50.86155, Longitude = -0.08361 };
            double distanceKm = 2;
            var responseList =
                await _sampleDataLocationsService.GetLocationsWithinBoundary(location, distanceKm, "ppl", null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 2);

            Assert.AreEqual(response[0].Id, 2649716);
            Assert.AreEqual(response[0].Name, "Falmer");

            Assert.AreEqual(response[1].Id, 2637064);
            Assert.AreEqual(response[1].Name, "Stanmer");
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundaryAndFeature_NotFound_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 30.033333, Longitude = 31.233334 };
            double distanceKm = 2;
            var responseList =
                await _sampleDataLocationsService.GetLocationsWithinBoundary(location, distanceKm, "ppl", null, null, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 0);
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundaryAndFeature_Count_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 50.86155, Longitude = -0.08361 };
            double distanceKm = 2;
            var response = await _sampleDataLocationsService.GetLocationsWithinBoundaryCount(location, distanceKm, "ppl");

            Assert.AreEqual(response, 2);
        }

        [TestMethod]
        public async Task GetLocationsWithInBoundaryAndFeature_Count_NotFound_Test()
        {
            GeoCoordinates location = new GeoCoordinates { Latitude = 30.033333, Longitude = 31.233334 };
            double distanceKm = 2;
            var response = await _sampleDataLocationsService.GetLocationsWithinBoundaryCount(location, distanceKm, "ppl");

            Assert.AreEqual(response, 0);
        }

        [TestMethod]
        public async Task GetLocations_Paged_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            //By default the list should be returned sorted by Name in acsending order
            Assert.AreEqual(response[0].Id, 9256358);
            Assert.AreEqual(response[0].Name, "American Express Community Stadium");

            Assert.AreEqual(response[1].Id, 2655786);
            Assert.AreEqual(response[1].Name, "Bevendean");

            Assert.AreEqual(response[2].Id, 6289023);
            Assert.AreEqual(response[2].Name, "Brighton General Hospital");

            Assert.AreEqual(response[3].Id, 8378783);
            Assert.AreEqual(response[3].Name, "Brighton Racecourse");

            Assert.AreEqual(response[4].Id, 3333133);
            Assert.AreEqual(response[4].Name, "Brighton and Hove");

            offset += limit;

            responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 2651208);
            Assert.AreEqual(response[0].Name, "Ditchling Beacon");

            Assert.AreEqual(response[1].Id, 7302952);
            Assert.AreEqual(response[1].Name, "Egyptian Museum");

            Assert.AreEqual(response[2].Id, 2649716);
            Assert.AreEqual(response[2].Name, "Falmer");

            Assert.AreEqual(response[3].Id, 6945667);
            Assert.AreEqual(response[3].Name, "Falmer Railway Station");

            Assert.AreEqual(response[4].Id, 10296363);
            Assert.AreEqual(response[4].Name, "Grand Ocean");

            offset += limit;

            responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 6289021);
            Assert.AreEqual(response[0].Name, "Hanover Crescent");

            Assert.AreEqual(response[1].Id, 2645491);
            Assert.AreEqual(response[1].Name, "Kingley Vale");

            Assert.AreEqual(response[2].Id, 7300473);
            Assert.AreEqual(response[2].Name, "Kingston Near Lewes");

            Assert.AreEqual(response[3].Id, 6945665);
            Assert.AreEqual(response[3].Name, "London Road Railway Station");

            Assert.AreEqual(response[4].Id, 2634959);
            Assert.AreEqual(response[4].Name, "London Victoria station");

            offset += limit;

            responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 6543954);
            Assert.AreEqual(response[0].Name, "Longhill High School");

            Assert.AreEqual(response[1].Id, 6945666);
            Assert.AreEqual(response[1].Name, "Moulsecoomb Railway Station");

            Assert.AreEqual(response[2].Id, 6464958);
            Assert.AreEqual(response[2].Name, "Old Ship Hotel");

            Assert.AreEqual(response[3].Id, 2640582);
            Assert.AreEqual(response[3].Name, "Patcham");

            Assert.AreEqual(response[4].Id, 2640206);
            Assert.AreEqual(response[4].Name, "Plumpton");


            offset += limit;

            responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 355017);
            Assert.AreEqual(response[0].Name, "Pyramids of Giza");

            Assert.AreEqual(response[1].Id, 6502992);
            Assert.AreEqual(response[1].Name, "Queens Hotel");

            Assert.AreEqual(response[2].Id, 6945127);
            Assert.AreEqual(response[2].Name, "Royal Victoria DLR Station");

            Assert.AreEqual(response[3].Id, 7298481);
            Assert.AreEqual(response[3].Name, "St. Ann (Without)");

            Assert.AreEqual(response[4].Id, 2637064);
            Assert.AreEqual(response[4].Name, "Stanmer");

            offset += limit;

            responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            response = responseList.ToList();

            Assert.AreEqual(response.Count, 4);

            Assert.AreEqual(response[0].Id, 9884128);
            Assert.AreEqual(response[0].Name, "The Cockshut");

            Assert.AreEqual(response[1].Id, 10296359);
            Assert.AreEqual(response[1].Name, "The Newmarket Inn");

            Assert.AreEqual(response[2].Id, 6695975);
            Assert.AreEqual(response[2].Name, "Victoria Underground Station");

            Assert.AreEqual(response[3].Id, 6690584);
            Assert.AreEqual(response[3].Name, "Woodingdean");
        }

        [TestMethod]
        public async Task GetLocations_OrderedById_Asc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "id");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 355017);
            Assert.AreEqual(response[0].Name, "Pyramids of Giza");

            Assert.AreEqual(response[1].Id, 2634959);
            Assert.AreEqual(response[1].Name, "London Victoria station");

            Assert.AreEqual(response[2].Id, 2637064);
            Assert.AreEqual(response[2].Name, "Stanmer");

            Assert.AreEqual(response[3].Id, 2640206);
            Assert.AreEqual(response[3].Name, "Plumpton");

            Assert.AreEqual(response[4].Id, 2640582);
            Assert.AreEqual(response[4].Name, "Patcham");
        }

        [TestMethod]
        public async Task GetLocations_OrderedById_Desc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "-id");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 10296363);
            Assert.AreEqual(response[0].Name, "Grand Ocean");

            Assert.AreEqual(response[1].Id, 10296359);
            Assert.AreEqual(response[1].Name, "The Newmarket Inn");

            Assert.AreEqual(response[2].Id, 9884128);
            Assert.AreEqual(response[2].Name, "The Cockshut");

            Assert.AreEqual(response[3].Id, 9256358);
            Assert.AreEqual(response[3].Name, "American Express Community Stadium");

            Assert.AreEqual(response[4].Id, 8378783);
            Assert.AreEqual(response[4].Name, "Brighton Racecourse");
        }

        [TestMethod]
        public async Task GetLocations_OrderedByName_Asc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 9256358);
            Assert.AreEqual(response[0].Name, "American Express Community Stadium");

            Assert.AreEqual(response[1].Id, 2655786);
            Assert.AreEqual(response[1].Name, "Bevendean");

            Assert.AreEqual(response[2].Id, 6289023);
            Assert.AreEqual(response[2].Name, "Brighton General Hospital");

            Assert.AreEqual(response[3].Id, 8378783);
            Assert.AreEqual(response[3].Name, "Brighton Racecourse");

            Assert.AreEqual(response[4].Id, 3333133);
            Assert.AreEqual(response[4].Name, "Brighton and Hove");
        }

        [TestMethod]
        public async Task GetLocations_OrderedByName_Desc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "-name");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 6690584);
            Assert.AreEqual(response[0].Name, "Woodingdean");

            Assert.AreEqual(response[1].Id, 6695975);
            Assert.AreEqual(response[1].Name, "Victoria Underground Station");

            Assert.AreEqual(response[2].Id, 10296359);
            Assert.AreEqual(response[2].Name, "The Newmarket Inn");

            Assert.AreEqual(response[3].Id, 9884128);
            Assert.AreEqual(response[3].Name, "The Cockshut");

            Assert.AreEqual(response[4].Id, 2637064);
            Assert.AreEqual(response[4].Name, "Stanmer");
        }

        [TestMethod]
        public async Task GetLocations_OrderedByLatitude_Asc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "lat");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 355017);
            Assert.AreEqual(response[0].Name, "Pyramids of Giza");
            Assert.AreEqual(response[0].Coordinates.Latitude, 29.98333);
            Assert.AreEqual(response[0].Coordinates.Longitude, 31.13333);

            Assert.AreEqual(response[1].Id, 7302952);
            Assert.AreEqual(response[1].Name, "Egyptian Museum");
            Assert.AreEqual(response[1].Coordinates.Latitude, 30.04796);
            Assert.AreEqual(response[1].Coordinates.Longitude, 31.23368);

            Assert.AreEqual(response[2].Id, 6543954);
            Assert.AreEqual(response[2].Name, "Longhill High School");
            Assert.AreEqual(response[2].Coordinates.Latitude, 50.81787);
            Assert.AreEqual(response[2].Coordinates.Longitude, -0.06824);

            Assert.AreEqual(response[3].Id, 6289021);
            Assert.AreEqual(response[3].Name, "Hanover Crescent");
            Assert.AreEqual(response[3].Coordinates.Latitude, 50.83084);
            Assert.AreEqual(response[3].Coordinates.Longitude, -0.13057);

            Assert.AreEqual(response[4].Id, 6289023);
            Assert.AreEqual(response[4].Name, "Brighton General Hospital");
            Assert.AreEqual(response[4].Coordinates.Latitude, 50.83092);
            Assert.AreEqual(response[4].Coordinates.Longitude, -0.11417);
        }

        [TestMethod]
        public async Task GetLocations_OrderedByLatitude_Desc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "-lat");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 6945127);
            Assert.AreEqual(response[0].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[0].Coordinates.Latitude, 51.50917);
            Assert.AreEqual(response[0].Coordinates.Longitude, 0.01798);

            Assert.AreEqual(response[1].Id, 6695975);
            Assert.AreEqual(response[1].Name, "Victoria Underground Station");
            Assert.AreEqual(response[1].Coordinates.Latitude, 51.49586);
            Assert.AreEqual(response[1].Coordinates.Longitude, -0.14354);

            Assert.AreEqual(response[2].Id, 2634959);
            Assert.AreEqual(response[2].Name, "London Victoria station");
            Assert.AreEqual(response[2].Coordinates.Latitude, 51.49571);
            Assert.AreEqual(response[2].Coordinates.Longitude, -0.14437);

            Assert.AreEqual(response[3].Id, 2640206);
            Assert.AreEqual(response[3].Name, "Plumpton");
            Assert.AreEqual(response[3].Coordinates.Latitude, 50.90539);
            Assert.AreEqual(response[3].Coordinates.Longitude, -0.07356);

            //Assert.AreEqual(response[4].Id, 2645491);
            //Assert.AreEqual(response[4].Name, "Kingley Vale");
            //Assert.AreEqual(response[4].Coordinates.Latitude, 50.9);
            //Assert.AreEqual(response[4].Coordinates.Longitude, -0.08333);

            Assert.AreEqual(response[4].Id, 2651208);
            Assert.AreEqual(response[4].Name, "Ditchling Beacon");
            Assert.AreEqual(response[4].Coordinates.Latitude, 50.9);
            Assert.AreEqual(response[4].Coordinates.Longitude, -0.1);
        }

        [TestMethod]
        public async Task GetLocations_OrderedByLongitude_Asc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "lon");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 2640582);
            Assert.AreEqual(response[0].Name, "Patcham");
            Assert.AreEqual(response[0].Coordinates.Latitude, 50.86396);
            Assert.AreEqual(response[0].Coordinates.Longitude, -0.15003);

            Assert.AreEqual(response[1].Id, 2634959);
            Assert.AreEqual(response[1].Name, "London Victoria station");
            Assert.AreEqual(response[1].Coordinates.Latitude, 51.49571);
            Assert.AreEqual(response[1].Coordinates.Longitude, -0.14437);

            Assert.AreEqual(response[2].Id, 6695975);
            Assert.AreEqual(response[2].Name, "Victoria Underground Station");
            Assert.AreEqual(response[2].Coordinates.Latitude, 51.49586);
            Assert.AreEqual(response[2].Coordinates.Longitude, -0.14354);

            Assert.AreEqual(response[3].Id, 6945665);
            Assert.AreEqual(response[3].Name, "London Road Railway Station");
            Assert.AreEqual(response[3].Coordinates.Latitude, 50.83684);
            Assert.AreEqual(response[3].Coordinates.Longitude, -0.13605);

            Assert.AreEqual(response[4].Id, 3333133);
            Assert.AreEqual(response[4].Name, "Brighton and Hove");
            Assert.AreEqual(response[4].Coordinates.Latitude, 50.83333);
            Assert.AreEqual(response[4].Coordinates.Longitude, -0.13333);
        }

        [TestMethod]
        public async Task GetLocations_OrderedByLongitude_Desc_Test()
        {
            int offset = 0;
            const int limit = 5;

            var responseList = await _sampleDataLocationsService.Get(offset, limit, "-lon");
            var response = responseList.ToList();

            Assert.AreEqual(response.Count, 5);

            Assert.AreEqual(response[0].Id, 7302952);
            Assert.AreEqual(response[0].Name, "Egyptian Museum");
            Assert.AreEqual(response[0].Coordinates.Latitude, 30.04796);
            Assert.AreEqual(response[0].Coordinates.Longitude, 31.23368);

            Assert.AreEqual(response[1].Id, 355017);
            Assert.AreEqual(response[1].Name, "Pyramids of Giza");
            Assert.AreEqual(response[1].Coordinates.Latitude, 29.98333);
            Assert.AreEqual(response[1].Coordinates.Longitude, 31.13333);

            Assert.AreEqual(response[2].Id, 6945127);
            Assert.AreEqual(response[2].Name, "Royal Victoria DLR Station");
            Assert.AreEqual(response[2].Coordinates.Latitude, 51.50917);
            Assert.AreEqual(response[2].Coordinates.Longitude, 0.01798);

            Assert.AreEqual(response[3].Id, 9884128);
            Assert.AreEqual(response[3].Name, "The Cockshut");
            Assert.AreEqual(response[3].Coordinates.Latitude, 50.86074);
            Assert.AreEqual(response[3].Coordinates.Longitude, -0.01313);

            Assert.AreEqual(response[4].Id, 7300473);
            Assert.AreEqual(response[4].Name, "Kingston Near Lewes");
            Assert.AreEqual(response[4].Coordinates.Latitude, 50.85192);
            Assert.AreEqual(response[4].Coordinates.Longitude, -0.03276);

            //Assert.AreEqual(response[3].Id, 7298481);
            //Assert.AreEqual(response[3].Name, "St. Ann (Without)");
            //Assert.AreEqual(response[3].Coordinates.Latitude, 50.87128);
            //Assert.AreEqual(response[3].Coordinates.Longitude, -0.03609);

            //Assert.AreEqual(response[4].Id, 6543954);
            //Assert.AreEqual(response[4].Name, "Longhill High School");
            //Assert.AreEqual(response[4].Coordinates.Latitude, 50.81787);
            //Assert.AreEqual(response[4].Coordinates.Longitude, -0.06824);
        }

        [TestMethod]
        public async Task GetCountryCodeById_Test()
        {
            var response = await _locationsService.GetCountryCode("GB");

            Assert.AreEqual("GB", response.Id);
            Assert.AreEqual("United Kingdom of Great Britain and Northern Ireland", response.Name);
            Assert.AreEqual("GBR", response.Alpha3Code);
            Assert.AreEqual(826, response.NumericCode);
        }

        [TestMethod]
        public async Task GetCountryCodeById_NotFound_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => _locationsService.GetCountryCode("ZZ"));
        }

        [TestMethod]
        public async Task GetAllCountryCodes_Test()
        {
            var response = await _locationsService.GetCountryCodes();
            var responseList = response.ToList();

            Assert.AreEqual("EG", responseList[0].Id);
            Assert.AreEqual("Egypt", responseList[0].Name);
            Assert.AreEqual("EGY", responseList[0].Alpha3Code);
            Assert.AreEqual(818, responseList[0].NumericCode);

            Assert.AreEqual("GB", responseList[1].Id);
            Assert.AreEqual("United Kingdom of Great Britain and Northern Ireland", responseList[1].Name);
            Assert.AreEqual("GBR", responseList[1].Alpha3Code);
            Assert.AreEqual(826, responseList[1].NumericCode);
        }

        [TestMethod]
        public async Task GetFeatureCodeById_Test()
        {
            var response = await _locationsService.GetFeatureCode("PPL");

            Assert.AreEqual("PPL", response.Id);
            Assert.AreEqual("populated place", response.Name);
        }

        [TestMethod]
        public async Task GetFeatureCodeById_NotFound_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => _locationsService.GetFeatureCode("ZZZ"));
        }

        [TestMethod]
        public async Task GetAllFeatureCodes_Test()
        {
            var response = await _locationsService.GetFeatureCodes();
            var responseList = response.ToList();

            Assert.AreEqual("AIRP", responseList[0].Id);
            Assert.AreEqual("airport", responseList[0].Name);

            Assert.AreEqual("PPL", responseList[1].Id);
            Assert.AreEqual("populated place", responseList[1].Name);
        }

        private static async Task PopulateCountryCodes(LocationsContext locationsContext)
        {
            CountryCode countryCode = await locationsContext.CountryCodes.SingleOrDefaultAsync(x => x.Id == "GB");

            if (countryCode == null)
            {
                countryCode = new CountryCode
                {
                    Id = "GB",
                    Name = "United Kingdom of Great Britain and Northern Ireland",
                    Alpha3Code = "GBR",
                    NumericCode = 826
                };

                locationsContext.CountryCodes.Add(countryCode);
                await locationsContext.SaveChangesAsync();
            }

            countryCode = await locationsContext.CountryCodes.SingleOrDefaultAsync(x => x.Id == "EG");

            if (countryCode == null)
            {
                countryCode = new CountryCode
                {
                    Id = "EG",
                    Name = "Egypt",
                    Alpha3Code = "EGY",
                    NumericCode = 818
                };

                locationsContext.CountryCodes.Add(countryCode);
                await locationsContext.SaveChangesAsync();
            }
        }

        private static async Task PopulateCountryCodes(LocationsContext locationsContext, string inputFilename)
        {
            using (StreamReader streamReader = new StreamReader(inputFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();   //Ignore first line which is just the headers
                line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    string[] components = line.Split('\t');

                    string name = components[0];
                    name = name.Trim();

                    string alpha2Code = components[1];
                    alpha2Code = alpha2Code.Trim();

                    string alpha3Code = components[2];
                    alpha3Code = alpha3Code.Trim();

                    string numeric = components[3];
                    numeric = numeric.Trim();
                    int numericCode = Convert.ToInt32(numeric);

                    Console.WriteLine($"{name}");

                    CountryCode countryCode = await locationsContext
                        .CountryCodes
                        .SingleOrDefaultAsync(x => x.Id == alpha2Code);

                    if (countryCode == null)
                    {
                        countryCode = new CountryCode
                        {
                            Id = alpha2Code,
                            Name = name,
                            Alpha3Code = alpha3Code,
                            NumericCode = numericCode
                        };

                        locationsContext.CountryCodes.Add(countryCode);
                    }
                    else
                    {
                        countryCode.Name = name;
                        countryCode.Alpha3Code = alpha3Code;
                        countryCode.NumericCode = numericCode;
                        locationsContext.CountryCodes.Update(countryCode);
                    }

                    line = await streamReader.ReadLineAsync();
                }

            }

            await locationsContext.SaveChangesAsync();
        }

        private static async Task PopulateFeatureCodes(LocationsContext locationsContext)
        {
            FeatureCode featureCode = await locationsContext.FeatureCodes.SingleOrDefaultAsync(x => x.Id == "PPL");

            if (featureCode == null)
            {
                featureCode = new FeatureCode
                {
                    Id = "PPL",
                    Name = "populated place"
                };

                locationsContext.FeatureCodes.Add(featureCode);
                await locationsContext.SaveChangesAsync();
            }

            featureCode = await locationsContext.FeatureCodes.SingleOrDefaultAsync(x => x.Id == "AIRP");

            if (featureCode == null)
            {
                featureCode = new FeatureCode
                {
                    Id = "AIRP",
                    Name = "airport"
                };

                locationsContext.FeatureCodes.Add(featureCode);
                await locationsContext.SaveChangesAsync();
            }
        }

        private static async Task PopulateFeatureCodes(LocationsContext locationsContext, string inputFilename)
        {
            using (StreamReader streamReader = new StreamReader(inputFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    if (line.Length > 0)
                    {
                        string[] components = line.Split('\t');

                        string[] featureComponents = components[0].Split('.');
                        //string class = featureComponents[0];   //Ignore feature class for now
                        string code = featureComponents[1];   //We use the feature code for the ID
                        string name = components[1];

                        Console.WriteLine($"{code}");

                        FeatureCode featureCode = await locationsContext
                            .FeatureCodes
                            .SingleOrDefaultAsync(x => x.Id == code);

                        if (featureCode == null)
                        {
                            featureCode = new FeatureCode
                            {
                                Id = code,
                                Name = name
                            };

                            locationsContext.Add(featureCode);
                        }
                        else
                        {
                            featureCode.Name = name;

                            locationsContext.Update(featureCode);
                        }

                    }

                    line = await streamReader.ReadLineAsync();
                }
            }

            await locationsContext.SaveChangesAsync();
        }

        private static async Task PopulateSeedData(LocationsContext locationsContext, string inputFilename)
        {
            using (StreamReader streamReader = new StreamReader(inputFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    string[] components = line.Split('\t');

                    //See: http://download.geonames.org/export/dump/readme.txt
                    long id = Convert.ToInt64(components[0]);
                    string name = components[1];
                    double latitude = Convert.ToDouble(components[4]);
                    double longitude = Convert.ToDouble(components[5]);
                    string featureClass = components[6];
                    string featureCode = components[7];
                    string countryCode = components[8];

                    CountryCode curCountryCode = await locationsContext.CountryCodes.SingleOrDefaultAsync(x => x.Id == countryCode);
                    if (curCountryCode == null)
                        throw new Exception("Failed to find country code");

                    FeatureCode curFeatureCode = await locationsContext.FeatureCodes.SingleOrDefaultAsync(x => x.Id == featureCode);
                    if (curFeatureCode == null)
                        throw new Exception("Failed to find feature code");

                    Location location = new Location
                    {
                        Id = id,
                        Name = name,
                        CountryCode = curCountryCode,
                        FeatureCode = curFeatureCode,
                        Latitude = latitude,
                        Longitude = longitude
                    };

                    locationsContext.Add(location);

                    line = await streamReader.ReadLineAsync();
                }

                await locationsContext.SaveChangesAsync();
            }
        }
    }
}
