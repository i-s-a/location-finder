This is a simple RESTful API that can support managing and querying various locations around the world for their latitude and longitude. It also supports querying for the nearest locations within a fixed radius, expressed as a distance in Km, centred around a specific latitude and longitude.

The RESTful API is implemented in .NET Core 2.2 WebAPI and database access uses Entity Framework Core. By default the locations are stored in a SQLite database but the API can easily be configued to support any SQL database.

An example of the RESTful API, currently populated with locations for the United Kingdom and Northern Ireland, is available at https://location-finder-100.herokuapp.com/. Swagger documentation for the API is available at https://location-finder-100.herokuapp.com/swagger/. The format of the data matches a subset of the format used by the [GeoNames](https://www.geonames.org/) website which contains a dataset of millions of locations from around the world that can be downloaded for free.

As an example of the API in use, a very simple website that can run some of the queries supported by the API is available at https://location-finder-client-100.herokuapp.com/. The website is implemented using React and the code for the website is available at https://bitbucket.org/i-s-a/location-finder-client/. 

The [Location Finder Utility project](https://bitbucket.org/i-s-a/location-finder-utility/src/master/) provides tools that allow you to populate locations to create your own database and also contains a .NET Core client connection component that can be used to access the RESTful API directly. 
