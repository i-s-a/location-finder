# NuGet restore
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /src

COPY *.sln .
COPY GeoMath/*.csproj ./GeoMath/
COPY LocationFinderTest/*.csproj ./LocationFinderTest/
COPY LocationFinder/*.csproj ./LocationFinder/
RUN dotnet restore

# publish
COPY GeoMath/. ./GeoMath/
COPY LocationFinder/. ./LocationFinder/
WORKDIR /src/LocationFinder
RUN dotnet publish -c Release -o /app --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
WORKDIR /app
COPY --from=build /app ./
# ENTRYPOINT ["dotnet", "LocationFinder.dll"]
# heroku uses the following
CMD ASPNETCORE_URLS=http://*:$PORT dotnet LocationFinder.dll