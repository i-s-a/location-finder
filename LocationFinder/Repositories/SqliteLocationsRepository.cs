﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LocationFinder.Models;
using GeoMathLib;
using System.Text;

namespace LocationFinder.Repositories
{
    public class SqliteLocationsRepository : ILocationsRepository<long>
    {
        private readonly LocationsContext _locationsContext = null;

        public SqliteLocationsRepository(LocationsContext context)
        {
            _locationsContext = context;
        }

        public async Task Add(Location location)
        {
            _locationsContext.Locations.Add(location);
            await _locationsContext.SaveChangesAsync();
        }

        public async Task Delete(Location location)
        {
            _locationsContext.Locations.Remove(location);
            await _locationsContext.SaveChangesAsync();
        }

        public async Task Update(Location location)
        {
            _locationsContext.Locations.Update(location);
            await _locationsContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Location>> Get(int offset, int limit, string sortColumn, bool descending)
        {
            if (descending)
            {
                var locations = await _locationsContext.Locations
                    .OrderByPropertyDescending(sortColumn)
                    .Include(c => c.CountryCode)
                    .Include(f => f.FeatureCode)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();

                return locations;
            }
            else
            {
                var locations = await _locationsContext.Locations
                    .OrderByProperty(sortColumn)
                    .Include(c => c.CountryCode)
                    .Include(f => f.FeatureCode)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();

                return locations;
            }
        }

        public async Task<Location> GetById(long id)
        {
            var location = await _locationsContext
                .Locations
                .Include(c => c.CountryCode)
                .Include(f => f.FeatureCode)
                .SingleOrDefaultAsync(x => x.Id == id);

            return location;
        }
        
        public async Task<IEnumerable<Location>> GetByName(string name, string countryCodeId, int offset, 
            int limit, string sortColumn, bool descending)
        {
            string lowerCaseName = name.ToLower();

            if (string.IsNullOrEmpty(countryCodeId))
            {
                if (descending)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.Name.ToLower() == lowerCaseName)
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.Name.ToLower() == lowerCaseName)
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
            else
            {
                string upperCaseCountryCodeId = countryCodeId.ToUpper();

                if (descending)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => 
                                x.Name.ToLower() == lowerCaseName &&
                                x.CountryCode.Id == upperCaseCountryCodeId)
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                                x.Name.ToLower() == lowerCaseName &&
                                x.CountryCode.Id == upperCaseCountryCodeId)
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
        }

        public async Task<IEnumerable<Location>> GetSimilarByName(string name, string countryCodeId, 
            int offset, int limit, string sortColumn, bool descending)
        {
            string lowerCaseName = name.ToLower();

            string[] names = lowerCaseName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            StringBuilder parameterValue = new StringBuilder();

            foreach (var n in names)
            {
                parameterValue.Append("%");
                parameterValue.Append(n);
            }

            parameterValue.Append("%");

            if (string.IsNullOrEmpty(countryCodeId))
            {
                if (descending)
                {
                    //TODO: Is there a better way to simulate a LIKE query in EF using LINQ without
                    //using a raw SQL query?? Doing it this way is proving very inefficient.
                    //var locations = await _locationsContext.Locations
                    //    .Where(x => x.Name.ContainsAll(names))
                    //    .OrderByPropertyDescending(sortColumn)
                    //    .Include(c => c.CountryCode)
                    //    .Include(f => f.FeatureCode)
                    //    .Skip(offset)
                    //    .Take(limit)
                    //    .ToListAsync();
                    var locations = await _locationsContext.Locations
                        .FromSql("SELECT * FROM Locations WHERE Name LIKE {0}", parameterValue.ToString())
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    //TODO: Is there a better way to simulate a LIKE query in EF using LINQ without
                    //using a raw SQL query?? Doing it this way is proving very inefficient.                    //var locations = await _locationsContext.Locations
                    //    .Where(x => x.Name.ContainsAll(names))
                    //    .OrderByProperty(sortColumn)
                    //    .Include(c => c.CountryCode)
                    //    .Include(f => f.FeatureCode)
                    //    .Skip(offset)
                    //    .Take(limit)
                    //    .ToListAsync();
                    var locations = await _locationsContext.Locations
                        .FromSql("SELECT * FROM Locations WHERE Name LIKE {0}", parameterValue.ToString())
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
            else
            {
                if (descending)
                {
                    var locations = await _locationsContext.Locations
                        .FromSql("SELECT * FROM Locations WHERE Name LIKE {0} AND CountryCodeId = {1}",
                            parameterValue.ToString(), countryCodeId)
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .FromSql("SELECT * FROM Locations WHERE Name LIKE {0} AND CountryCodeId = {1}",
                            parameterValue.ToString(), countryCodeId)
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
        }

        public async Task<IEnumerable<Location>> GetByFeatureCodeId(string id, string countryCodeId,
            int offset, int limit, string sortColumn, bool descending)
        {
            string upperCaseId = id.ToUpper();

            if (string.IsNullOrEmpty(countryCodeId))
            {
                if (descending)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == upperCaseId)
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == upperCaseId)
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
            else
            {
                string upperCaseCountryCodeId = countryCodeId.ToUpper();

                if (descending)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                                x.FeatureCode.Id == upperCaseId &&
                                x.CountryCode.Id == upperCaseCountryCodeId)
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                                x.FeatureCode.Id == upperCaseId &&
                                x.CountryCode.Id == upperCaseCountryCodeId)
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .Skip(offset)
                        .Take(limit)
                        .ToListAsync();

                    return locations;
                }
            }
        }

        public async Task<IEnumerable<Location>> GetByCountryCodeId(string id, int offset,
            int limit, string sortColumn, bool descending)
        {
            string upperCaseId = id.ToUpper();

            if (descending)
            {
                var locations = await _locationsContext.Locations
                    .Where(x => x.CountryCode.Id == upperCaseId)
                    .OrderByPropertyDescending(sortColumn)
                    .Include(c => c.CountryCode)
                    .Include(f => f.FeatureCode)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();

                return locations;
            }
            else
            {
                var locations = await _locationsContext.Locations
                    .Where(x => x.CountryCode.Id == upperCaseId)
                    .OrderByProperty(sortColumn)
                    .Include(c => c.CountryCode)
                    .Include(f => f.FeatureCode)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();

                return locations;
            }
        }

        public async Task<int> GetByNameCount(string name, string countryCodeId, bool includeSimilarNames)
        {
            if (string.IsNullOrEmpty(name))
            {
                return await _locationsContext.Locations.CountAsync();
            }
            else
            {
                string lowerCaseName = name.ToLower();

                if (includeSimilarNames)
                {
                    string[] names = lowerCaseName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    StringBuilder parameterValue = new StringBuilder();

                    foreach (var n in names)
                    {
                        parameterValue.Append("%");
                        parameterValue.Append(n);
                    }

                    parameterValue.Append("%");

                    if (string.IsNullOrEmpty(countryCodeId))
                    {
                        return await _locationsContext
                            .Locations
                            .FromSql("SELECT * FROM Locations WHERE Name LIKE {0}", parameterValue.ToString())
                            .CountAsync();
                    }
                    else
                    {
                        //string upperCaseCountryCodeId = countryCodeId.ToUpper();
                        //return await _locationsContext
                        //    .Locations
                        //    .Where(x => x.CountryCode.Id == upperCaseCountryCodeId)
                        //    .CountAsync(x => x.Name.ContainsAll(names));

                        return await _locationsContext
                            .Locations
                            .FromSql("SELECT * FROM Locations WHERE Name LIKE {0} AND CountryCodeId = {1}",
                                parameterValue.ToString(), countryCodeId)
                            .CountAsync();
                    }

                }
                else
                {
                    if (string.IsNullOrEmpty(countryCodeId))
                    {
                        return await _locationsContext
                            .Locations
                            .CountAsync(x => x.Name.ToLower() == lowerCaseName);
                    }
                    else
                    {
                        string upperCaseCountryCodeId = countryCodeId.ToUpper();
                        return await _locationsContext
                            .Locations
                            .Where(x => x.CountryCode.Id == upperCaseCountryCodeId)
                            .CountAsync(x => x.Name.ToLower() == lowerCaseName);
                    }
                    
                }
            }
        }

        public async Task<int> GetByCodeCount(string countryCodeId, string featureCodeId)
        {
            if (!string.IsNullOrEmpty(countryCodeId) && !string.IsNullOrEmpty(featureCodeId))
            {
                return await _locationsContext
                    .Locations
                    .CountAsync(x =>
                        x.FeatureCode.Id == featureCodeId.ToUpper() &&
                        x.CountryCode.Id == countryCodeId.ToUpper());
            }
            else if (!string.IsNullOrEmpty(countryCodeId))
            {
                return await _locationsContext
                    .Locations
                    .CountAsync(x => x.CountryCode.Id == countryCodeId.ToUpper());
            }
            else if (!string.IsNullOrEmpty(featureCodeId))
            {
                return await _locationsContext
                    .Locations
                    .CountAsync(x => x.FeatureCode.Id == featureCodeId.ToUpper());
            }
            else
            {
                return 0;
            }
        }

        public async Task<IEnumerable<Location>> GetWithinBoundary(GeoCoordinates location,
            bool meridian180WithinDistance, BoundaryCoordinates boundary, string sortColumn,
            bool descending)
        {

            if (descending)
            {
                if (meridian180WithinDistance)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
            }
            else
            {
                if (meridian180WithinDistance)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x =>
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
            }
        }

        public async Task<int> GetWithinBoundaryCount(GeoCoordinates location,
            bool meridian180WithinDistance, BoundaryCoordinates boundary)
        {

            if (meridian180WithinDistance)
            {
                return await _locationsContext.Locations.CountAsync(x =>
                        (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                        (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude));
            }
            else
            {
                return await _locationsContext.Locations.CountAsync(x =>
                        (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                        (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude));
            }
        }

        public async Task<IEnumerable<Location>> GetWithinBoundaryByFeature(string featureCodeId,
            GeoCoordinates location, bool meridian180WithinDistance, BoundaryCoordinates boundary,
            string sortColumn, bool descending)
        {

            if (descending)
            {
                if (meridian180WithinDistance)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByPropertyDescending(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
            }
            else
            {
                if (meridian180WithinDistance)
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
                else
                {
                    var locations = await _locationsContext.Locations
                        .Where(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                            (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                            (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude))
                        .OrderByProperty(sortColumn)
                        .Include(c => c.CountryCode)
                        .Include(f => f.FeatureCode)
                        .ToListAsync();

                    return locations;
                }
            }
        }

        public async Task<int> GetWithinBoundaryCountByFeature(string featureCodeId,
            GeoCoordinates location, bool meridian180WithinDistance, BoundaryCoordinates boundary)
        {

            if (meridian180WithinDistance)
            {
                return await _locationsContext.Locations.CountAsync(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                        (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                        (x.Longitude >= boundary.MinCoordinates.Longitude || x.Longitude <= boundary.MaxCoordinates.Longitude));
            }
            else
            {
                return await _locationsContext.Locations.CountAsync(x => x.FeatureCode.Id == featureCodeId.ToUpper() &&
                        (x.Latitude >= boundary.MinCoordinates.Latitude && x.Latitude <= boundary.MaxCoordinates.Latitude) &&
                        (x.Longitude >= boundary.MinCoordinates.Longitude && x.Longitude <= boundary.MaxCoordinates.Longitude));
            }
        }

        public async Task<IEnumerable<CountryCode>> GetAllCountryCodes(string sortColumn, bool descending)
        {
            if (descending)
            {
                return await _locationsContext
                    .CountryCodes
                    .OrderByPropertyDescending(sortColumn)
                    .ToListAsync();
            }
            else
            {
                return await _locationsContext
                    .CountryCodes
                    .OrderByProperty(sortColumn)
                    .ToListAsync();
            }
        }

        public async Task<CountryCode> GetCountryCode(string id)
        {
            return await _locationsContext.CountryCodes.SingleOrDefaultAsync(x => x.Id.ToLower() == id.ToLower());
        }

        public async Task<IEnumerable<FeatureCode>> GetAllFeatureCodes(string sortColumn, bool descending)
        {
            if (descending)
            {
                return await _locationsContext
                    .FeatureCodes
                    .OrderByPropertyDescending(sortColumn)
                    .ToListAsync();
            }
            else
            {
                return await _locationsContext
                    .FeatureCodes
                    .OrderByProperty(sortColumn)
                    .ToListAsync();
            }
        }

        public async Task<FeatureCode> GetFeatureCode(string id)
        {
            return await _locationsContext.FeatureCodes.SingleOrDefaultAsync(x => x.Id.ToLower() == id.ToLower());
        }

    }
}
