﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LocationFinder.Models;
using GeoMathLib;

namespace LocationFinder.Repositories
{
    public interface ILocationsRepository<T>
    {
        Task Add(Location location);
        Task Delete(Location location);
        Task Update(Location location);

        Task<IEnumerable<Location>> Get(int offset, int limit, string sortColumn, bool descending);
        Task<Location> GetById(T id);
        Task<IEnumerable<Location>> GetByName(string name, string countryCodeId, int offset, int limit, 
            string sortColumn, bool descending);
        Task<IEnumerable<Location>> GetSimilarByName(string name, string countryCodeId, int offset, int limit,
            string sortColumn, bool descending);
        Task<IEnumerable<Location>> GetByFeatureCodeId(string id, string countryCodeId,
            int offset, int limit, string sortColumn, bool descending);
        Task<IEnumerable<Location>> GetByCountryCodeId(string id, int offset, int limit,
            string sortColumn, bool descending);

        Task<int> GetByNameCount(string name, string countryCodeId, bool includeSimilarNames);
        Task<int> GetByCodeCount(string countryCodeId, string featureCodeId);

        Task<IEnumerable<Location>> GetWithinBoundary(GeoCoordinates location,
            bool meridian180WithinDistance, BoundaryCoordinates boundary, string sortColumn,
            bool descending);
        Task<int> GetWithinBoundaryCount(GeoCoordinates location,
            bool meridian180WithinDistance, BoundaryCoordinates boundary);

        Task<IEnumerable<Location>> GetWithinBoundaryByFeature(string featureCodeId,
            GeoCoordinates location, bool meridian180WithinDistance, BoundaryCoordinates boundary,
            string sortColumn, bool descending);
        Task<int> GetWithinBoundaryCountByFeature(string featureCodeId, GeoCoordinates location,
            bool meridian180WithinDistance, BoundaryCoordinates boundary);

        Task<IEnumerable<CountryCode>> GetAllCountryCodes(string sortColumn, bool descending);
        Task<CountryCode> GetCountryCode(string id);
        Task<IEnumerable<FeatureCode>> GetAllFeatureCodes(string sortColumn, bool descending);
        Task<FeatureCode> GetFeatureCode(string id);
    }
}
