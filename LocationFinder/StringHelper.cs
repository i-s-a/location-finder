﻿namespace LocationFinder
{
    public static class StringHelper
    {
        public static bool ContainsAny(this string str, params string[] stringList)
        {
            string lowerCaseStr = str.ToLower();

            foreach (var s in stringList)
            {
                if (lowerCaseStr.Contains(s))
                    return true;
            }

            return false;
        }

        public static bool ContainsAll(this string str, params string[] stringList)
        {
            string lowerCaseStr = str.ToLower();

            foreach(var s in stringList)
            {
                if (!lowerCaseStr.Contains(s))
                    return false;
            }

            return true;
        }
    }
}
