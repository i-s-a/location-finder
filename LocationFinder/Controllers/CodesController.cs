﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LocationFinder.Services;

namespace LocationFinder.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CodesController : ControllerBase
    {
        private readonly LocationsService _locationsService = null;

        public CodesController(LocationsService locationsService)
        {
            _locationsService = locationsService;
        }

        // GET: api/v1/codes/countries
        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries()
        {
            try
            {
                var response = await _locationsService.GetCountryCodes();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/v1/codes/countries/gb
        [HttpGet("countries/{id}", Name = "Get")]
        public async Task<IActionResult> GetCountryCodeById(string id)
        {
            try
            {
                var response = await _locationsService.GetCountryCode(id);

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/v1/codes/features
        [HttpGet("features")]
        public async Task<IActionResult> GetFeatures()
        {
            try
            {
                var response = await _locationsService.GetFeatureCodes();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        // GET: api/v1/codes/features/ppl
        [HttpGet("features/{id}")]
        public async Task<IActionResult> GetFeatureCodeById(string id)
        {
            try
            {
                var response = await _locationsService.GetFeatureCode(id);

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //// POST: api/Codes
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Codes/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
