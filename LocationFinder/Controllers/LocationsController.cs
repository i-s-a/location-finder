﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LocationFinder.Services;
using LocationFinder.Requests;
using GeoMathLib;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LocationFinder.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LocationsController : Controller
    {
        private readonly LocationsService _locationsService = null;

        public LocationsController(LocationsService locationsService)
        {
            _locationsService = locationsService;
        }

        // GET api/v1/locations?offset=40&limit=20
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&offset=40&limit=20
        //
        // GET api/v1/locations?offset=40&limit=20&sort=id
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&sort=-lat
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&sort=+lon
        //
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&featureid=ppl
        // GET api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&featureid=ppl&sort=+name
        [HttpGet]
        public async Task<IActionResult> Get(double? lat, double? lon, double? distance, string featureid, int? offset, 
            int? limit, string sort)
        {
            try
            {
                if (lat.HasValue && lon.HasValue && distance.HasValue)
                {
                    //Get locations within "distance" of "latitude" and "longitude"
                    var response = await _locationsService.GetLocationsWithinBoundary(new GeoCoordinates
                    {
                        Latitude = lat.Value,
                        Longitude = lon.Value
                    }, distance.Value, featureid, offset, limit, sort);

                    return Ok(response);
                }
                else
                {
                    var response = await _locationsService.Get(offset, limit, sort);

                    return Ok(response);
                }

            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // HEAD api/v1/locations
        // HEAD api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0
        // HEAD api/v1/locations?lat=15.156565&lon=14.048504&distance=5.0&featureid=ppl
        [HttpHead]
        public async Task<IActionResult> GetCount(double? lat, double? lon, double? distance, string featureid)
        {
            try
            {
                if (lat.HasValue && lon.HasValue && distance.HasValue)
                {
                    //Get count of locations within "distance" of "latitude" and "longitude"
                    var response = await _locationsService.GetLocationsWithinBoundaryCount(new GeoCoordinates
                    {
                        Latitude = lat.Value,
                        Longitude = lon.Value
                    }, distance.Value, featureid);

                    Response.Headers.Add("X-Total-Count", response.ToString());

                    return NoContent();
                    
                }
                else
                {
                    //Get count of all locations
                    var response = await _locationsService.GetByNameLocationsCount(null, null, false);

                    Response.Headers.Add("X-Total-Count", response.ToString());

                    return NoContent();
                }

            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/v1/locations/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(long id)
        {
            try
            {
                var response = await _locationsService.GetLocationById(id);

                return Ok(response);

            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/v1/locations/names/hyde%20park
        // GET api/v1/locations/names/hyde%20park?similar=true
        [HttpGet("names/{name}")]
        public async Task<IActionResult> GetByName(string name, bool? similar, int ?offset, int? limit, string sort)
        {
            try
            {
                if (similar.HasValue && similar.Value == true)
                {
                    //Get locations with a similar name
                    var response = await _locationsService.GetSimilarLocationsByName(name, null, offset, limit, sort);
                    return Ok(response);
                }
                else
                {
                    //Get locations with the same name
                    var response = await _locationsService.GetLocationByName(name, null, offset, limit, sort);
                    return Ok(response);
                }
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // HEAD api/v1/locations/names/hyde%20park
        // HEAD api/v1/locations/names/hyde%20park?similar=true
        [HttpHead("names/{name}")]
        public async Task<IActionResult> GetByNameCount(string name, bool? similar)
        {
            try
            {
                if (similar.HasValue && similar.Value == true)
                {
                    //Get count of locations with a similar name
                    var response = await _locationsService.GetByNameLocationsCount(name, null, true);
                    Response.Headers.Add("X-Total-Count", response.ToString());
                    return NoContent();
                }
                else
                {
                    //Get count of locations with the same name
                    var response = await _locationsService.GetByNameLocationsCount(name, null, false);
                    Response.Headers.Add("X-Total-Count", response.ToString());
                    return NoContent();
                }
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/v1/locations/names/apollo%20theatre/countries/gb
        // GET api/v1/locations/names/apollo%20theatre/countries/gb?similar=true
        [HttpGet("names/{name}/countries/{country_id}")]
        public async Task<IActionResult> GetByNameAndCountry(string name, string country_id, bool? similar, int? offset, int? limit, string sort)
        {
            try
            {
                if (similar.HasValue && similar.Value == true)
                {
                    //Get locations with a similar name
                    var response = await _locationsService.GetSimilarLocationsByName(name, country_id, offset, limit, sort);
                    return Ok(response);
                }
                else
                {
                    //Get locations with the same name
                    var response = await _locationsService.GetLocationByName(name, country_id, offset, limit, sort);
                    return Ok(response);
                }
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // HEAD api/v1/locations/names/apollo%20theatre/countries/gb
        // HEAD api/v1/locations/names/apollo%20theatre/countries/gb?similar=true
        [HttpHead("names/{name}/countries/{country_id}")]
        public async Task<IActionResult> GetByNameAndCountryCount(string name, string country_id, bool? similar)
        {
            try
            {
                if (similar.HasValue && similar.Value == true)
                {
                    //Get count of locations with a similar name
                    var response = await _locationsService.GetByNameLocationsCount(name, country_id, true);
                    Response.Headers.Add("X-Total-Count", response.ToString());
                    return NoContent();
                }
                else
                {
                    //Get count of locations with the same name
                    var response = await _locationsService.GetByNameLocationsCount(name, country_id, false);
                    Response.Headers.Add("X-Total-Count", response.ToString());
                    return NoContent();
                }
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/v1/locations
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NewLocationRequest request)
        {
            try
            {
                var response = await _locationsService.CreateLocation(request);

                response.HRef = "/locations/" + response.Id.ToString();

                return Created("/locations/" + response.Id.ToString(), response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/v1/locations/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody]UpdateLocationRequest request)
        {
            try
            {
                var response = await _locationsService.CreateOrUpdateLocation(id, request);

                response.HRef = "/locations/" + response.Id.ToString();

                return Ok(response);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PATCH api/v1/locations/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(long id, [FromBody]UpdateLocationRequest request)
        {
            try
            {
                var response = await _locationsService.UpdateLocation(id, request);

                response.HRef = "/locations/" + response.Id.ToString();

                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/locations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            try
            {
                await _locationsService.DeleteLocation(id);

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return all locations with a particular feature (e.g. all museums, all populated places, etc.)
        // GET: api/v1/locations/features/ppl
        [HttpGet("features/{feature_id}")]
        public async Task<IActionResult> GetByFeatureCodeId(string feature_id,
            int? offset, int? limit, string sort)
        {
            try
            {
                var response = await _locationsService
                    .GetLocationsByFeatureCodeId(feature_id, null, offset, limit, sort);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return the count of all locations with a particular feature (e.g. all museums, all populated
        //places, etc.)
        // HEAD: api/v1/locations/features/ppl
        [HttpHead("features/{feature_id}")]
        public async Task<IActionResult> GetByFeatureCodeIdCount(string feature_id)
        {
            try
            {
                var response = await _locationsService
                    .GetByCodeLocationsCount(null, feature_id);
                Response.Headers.Add("X-Total-Count", response.ToString());
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return all locations with a particular feature in a specific country (e.g. all museums in
        //Germany, all populated places in Great Britain, etc.)
        // GET: api/v1/locations/features/ppl/countries/gb
        [HttpGet("features/{feature_id}/countries/{country_id}")]
        public async Task<IActionResult> GetByFeatureCodeIdForCountry(string feature_id, string country_id,
            int? offset, int? limit, string sort)
        {
            try
            {
                var response = await _locationsService
                    .GetLocationsByFeatureCodeId(feature_id, country_id, offset, limit, sort);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return the count of all locations with a particular feature in a specific country (e.g. all
        //museums in Germany, all populated places in Great Britain, etc.)
        // HEAD: api/v1/locations/features/ppl/countries/gb
        [HttpHead("features/{feature_id}/countries/{country_id}")]
        public async Task<IActionResult> GetByFeatureCodeIdForCountryCount(string feature_id, string country_id)
        {
            try
            {
                var response = await _locationsService
                    .GetByCodeLocationsCount(country_id, feature_id);
                Response.Headers.Add("X-Total-Count", response.ToString());
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return all locations within a specific country (e.g. all locations within the United States,
        //etc.)
        // GET: api/v1/locations/countries/gb
        [HttpGet("countries/{country_id}")]
        public async Task<IActionResult> GetByCountryCodeId(string country_id, int? offset,
            int? limit, string sort)
        {
            try
            {
                var response = await _locationsService.GetLocationsByCountryCodeId(country_id,
                    offset, limit, sort);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //Return the count of all locations within a specific country (e.g. all locations within the
        //United States, etc.)
        // HEAD: api/v1/locations/countries/gb
        [HttpHead("countries/{country_id}")]
        public async Task<IActionResult> GetByCountryCodeIdCount(string country_id)
        {
            try
            {
                var response = await _locationsService
                    .GetByCodeLocationsCount(country_id, null);
                Response.Headers.Add("X-Total-Count", response.ToString());
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
