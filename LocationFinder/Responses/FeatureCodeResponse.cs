﻿namespace LocationFinder.Responses
{
    public class FeatureCodeResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
