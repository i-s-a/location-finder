﻿namespace LocationFinder.Responses
{
    public class CountryCodeResponse
    {
        public string Id { get; set; }          //Alpha-2 code. This will be the ID of the country code
        public string Name { get; set; }        //The English name of the country
        public string Alpha3Code { get; set; }  //Alpha-3 code
        public int NumericCode { get; set; }    //Numeric code of the country
    }
}
