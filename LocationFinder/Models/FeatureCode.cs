﻿using System.ComponentModel.DataAnnotations;

namespace LocationFinder.Models
{
    public class FeatureCode
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
