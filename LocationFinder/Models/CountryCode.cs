﻿using System.ComponentModel.DataAnnotations;

namespace LocationFinder.Models
{
    //See https://www.iso.org/iso-3166-country-codes.html
    public class CountryCode
    {
        [Key]
        public string Id { get; set; }          //Alpha-2 code. We will index the table on the 2 character country code
        public string Name { get; set; }        //The English name of the country
        public string Alpha3Code { get; set; }  //Alpha-3 code
        public int NumericCode { get; set; }    //Numeric code of the country
    }
}
