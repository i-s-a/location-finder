﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LocationFinder.Models;
using LocationFinder.Repositories;
using LocationFinder.Requests;
using LocationFinder.Responses;
using GeoMathLib;

namespace LocationFinder.Services
{
    public class LocationsService
    {
        private readonly ILocationsRepository<long> _locationsRepository;
        private const int DEFAULT_OFFSET = 0;
        private const int DEFAULT_LIMIT = 100;
        private const string DEFAULT_SORT_ORDER = "name";
        private const double EARTH_RADIUS_KM = 6371.01;
        //Round the latitude and longitude to 6 decimal places
        private const int GEO_DECIMAL_PLACES = 6;
        //Round the distance to 3 decimal places
        private const int DISTANCE_DECIMAL_PLACES = 3;

        public LocationsService(ILocationsRepository<long> locationsRepository)
        {
            _locationsRepository = locationsRepository;
        }

        public async Task<LocationResponse> CreateLocation(NewLocationRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Attempting to add a location with no name");

            if (string.IsNullOrEmpty(request.CountryCodeId))
                throw new ArgumentException("Attempting to add a location with no country code");

            if (string.IsNullOrWhiteSpace(request.FeatureCodeId))
                throw new ArgumentException("Attempting to add a location with no feature code");

            if (!GeoMath.ValidCoordinates(request.Coordinates))
                throw new ArgumentException($"Invalid coordinates. " +
                    $"Coordinates must be between Latitude: {GeoMath.MIN_LAT} - {GeoMath.MAX_LAT}, " +
                    $"Longitude: {GeoMath.MIN_LON} - {GeoMath.MAX_LON}");

            var countryCode = await _locationsRepository.GetCountryCode(request.CountryCodeId);
            if (countryCode == null)
                throw new ArgumentException("Invalid country code");

            var featureCode = await _locationsRepository.GetFeatureCode(request.FeatureCodeId);
            if (featureCode == null)
                throw new ArgumentException("Invalid feature code");

            var location = new Location
            {
                Id = 0, //Let the database auto generate the ID
                Name = request.Name,
                CountryCode = countryCode,
                FeatureCode = featureCode,
                Latitude = request.Coordinates.Latitude,
                Longitude = request.Coordinates.Longitude
            };

            await _locationsRepository.Add(location);

            var response = GetLocationResponse(location, 0.0);

            return response;
        }

        public async Task<LocationResponse> CreateOrUpdateLocation(long id, UpdateLocationRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Attempting to add/update a location with no name");

            if (string.IsNullOrEmpty(request.CountryCodeId))
                throw new ArgumentException("Attempting to add/update a location with no country code");

            if (string.IsNullOrWhiteSpace(request.FeatureCodeId))
                throw new ArgumentException("Attempting to add/update a location with no feature code");

            if (!request.Coordinates.HasValue)
                throw new ArgumentException("Attempting to add/update a location with no coordinates");

            if (!GeoMath.ValidCoordinates(request.Coordinates.Value))
                throw new ArgumentException($"Invalid coordinates. " +
                    $"Coordinates must be between Latitude: {GeoMath.MIN_LAT} - {GeoMath.MAX_LAT}, " +
                    $"Longitude: {GeoMath.MIN_LON} - {GeoMath.MAX_LON}");

            var countryCode = await _locationsRepository.GetCountryCode(request.CountryCodeId);
            if (countryCode == null)
                throw new ArgumentException("Invalid country code");

            var featureCode = await _locationsRepository.GetFeatureCode(request.FeatureCodeId);
            if (featureCode == null)
                throw new ArgumentException("Invalid feature code");

            var location = await _locationsRepository.GetById(id);
            if (location == null)
            {
                //Doesn't already exist, so add it as a new location
                location = new Location
                {
                    Id = id,
                    Name = request.Name,
                    CountryCode = countryCode,
                    FeatureCode = featureCode,
                    Latitude = request.Coordinates.Value.Latitude,
                    Longitude = request.Coordinates.Value.Longitude
                };

                await _locationsRepository.Add(location);
            }
            else
            {
                //Already exists so we do a full update
                location.Name = request.Name;
                location.CountryCode = countryCode;
                location.FeatureCode = featureCode;
                location.Latitude = request.Coordinates.Value.Latitude;
                location.Longitude = request.Coordinates.Value.Longitude;

                await _locationsRepository.Update(location);
            }

            var response = GetLocationResponse(location, 0.0);

            return response;
        }

        public async Task<LocationResponse> UpdateLocation(long id, UpdateLocationRequest request)
        {
            var location = await _locationsRepository.GetById(id);

            if (location == null)
                throw new NotFoundException("Location not found");

            //The location already exists so we will only update fields that have been
            //set in the request

            if (!string.IsNullOrEmpty(request.Name))
            {
                location.Name = request.Name;
            }

            if (!string.IsNullOrEmpty(request.CountryCodeId))
            {
                var countryCode = await _locationsRepository.GetCountryCode(request.CountryCodeId);
                location.CountryCode = countryCode ?? throw new ArgumentException("Invalid country code");
            }

            if (!string.IsNullOrEmpty(request.FeatureCodeId))
            {
                var featureCode = await _locationsRepository.GetFeatureCode(request.FeatureCodeId);
                location.FeatureCode = featureCode ?? throw new ArgumentException("Invalid feature code");
            }

            if (request.Coordinates.HasValue)
            {
                if (!GeoMath.ValidCoordinates(request.Coordinates.Value))
                    throw new ArgumentException($"Invalid coordinates. " +
                        $"Coordinates must be between Latitude: {GeoMath.MIN_LAT} - {GeoMath.MAX_LAT}, " +
                        $"Longitude: {GeoMath.MIN_LON} - {GeoMath.MAX_LON}");

                location.Latitude = request.Coordinates.Value.Latitude;
                location.Longitude = request.Coordinates.Value.Longitude;
            }

            await _locationsRepository.Update(location);

            var response = GetLocationResponse(location, 0.0);

            return response;
        }

        public async Task DeleteLocation(long id)
        {
            var location = await _locationsRepository.GetById(id);

            if (location == null)
                throw new NotFoundException("Location not found");

            await _locationsRepository.Delete(location);
        }

        public async Task<IEnumerable<LocationResponse>> Get(int? offset, int? limit, string sortOrder)
        {
            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var locations = await _locationsRepository.Get(offsetToUse, limitToUse, sortOrderToUse, descending);

            return GetLocationResponseList(locations);
        }

        public async Task<LocationResponse> GetLocationById(long id)
        {
            var location = await _locationsRepository.GetById(id);

            if (location == null)
                throw new NotFoundException("Location not found");

            return GetLocationResponse(location, 0.0);
        }

        public async Task<IEnumerable<LocationResponse>> GetLocationByName(string name, string countryCodeId, 
            int? offset, int? limit, string sortOrder)
        {
            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var locations = await _locationsRepository.GetByName(name, countryCodeId, offsetToUse, limitToUse, 
                sortOrderToUse, descending);

            return GetLocationResponseList(locations);
        }

        public async Task<IEnumerable<LocationResponse>> GetSimilarLocationsByName(string name, string countryCodeId,
            int? offset, int? limit, string sortOrder)
        {
            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var locations = await _locationsRepository.GetSimilarByName(name, countryCodeId, offsetToUse,
                limitToUse, sortOrderToUse, descending);

            return GetLocationResponseList(locations);
        }

        public async Task<IEnumerable<LocationResponse>> GetLocationsByFeatureCodeId(string id,
            string countryCodeId, int? offset, int? limit, string sortOrder)
        {
            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var locations =
                await _locationsRepository.GetByFeatureCodeId(id, countryCodeId, offsetToUse, limitToUse, sortOrderToUse, descending);

            return GetLocationResponseList(locations);
        }

        public async Task<IEnumerable<LocationResponse>> GetLocationsByCountryCodeId(string id,
            int? offset, int? limit, string sortOrder)
        {
            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var locations =
                await _locationsRepository.GetByCountryCodeId(id, offsetToUse, limitToUse, sortOrderToUse, descending);

            return GetLocationResponseList(locations);
        }

        public Task<int> GetByNameLocationsCount(string name, string countryCodeId, bool includeSimilarNames)
        {
            return _locationsRepository.GetByNameCount(name, countryCodeId, includeSimilarNames);
        }

        public Task<int> GetByCodeLocationsCount(string countryCodeId, string featureCodeId)
        {
            return _locationsRepository.GetByCodeCount(countryCodeId, featureCodeId);
        }

        public async Task<IEnumerable<LocationResponse>> GetLocationsWithinBoundary(GeoCoordinates location,
            double distanceKm, string featureCodeId, int? offset, int? limit, string sortOrder)
        {
            if (!GeoMath.ValidCoordinates(location))
                throw new ArgumentException($"Invalid coordinates. " +
                    $"Coordinates must be between Latitude: {GeoMath.MIN_LAT} - {GeoMath.MAX_LAT}, " +
                    $"Longitude: {GeoMath.MIN_LON} - {GeoMath.MAX_LON}");

            int offsetToUse = DEFAULT_OFFSET;
            int limitToUse = DEFAULT_LIMIT;

            if (offset.HasValue)
            {
                offsetToUse = offset.Value;
            }

            if (limit.HasValue)
            {
                limitToUse = limit.Value;
            }

            (string sortOrderToUse, bool descending) = GetSortOrderString(sortOrder);

            var boundaryCoordinates = GeoMath.BoundingCoordinates(location, distanceKm, EARTH_RADIUS_KM);
            bool meridian180WithinDistance =
                boundaryCoordinates.MinCoordinates.Longitude > boundaryCoordinates.MaxCoordinates.Longitude;

            IEnumerable<Location> locations = null;

            if (string.IsNullOrEmpty(featureCodeId))
            {
                locations = await _locationsRepository.GetWithinBoundary(location,
                    meridian180WithinDistance, boundaryCoordinates, sortOrderToUse, descending);
            }
            else
            {
                locations = await _locationsRepository.GetWithinBoundaryByFeature(featureCodeId, location,
                    meridian180WithinDistance, boundaryCoordinates, sortOrderToUse, descending);
            }

            List<LocationResponse> locationList = FilterLocationsByDistance(location,
                locations, distanceKm);

            return locationList.Skip(offsetToUse).Take(limitToUse);
        }

        public async Task<int> GetLocationsWithinBoundaryCount(GeoCoordinates location, double distanceKm, string featureCodeId)
        {
            if (!GeoMath.ValidCoordinates(location))
                throw new ArgumentException($"Invalid coordinates. " +
                    $"Coordinates must be between Latitude: {GeoMath.MIN_LAT} - {GeoMath.MAX_LAT}, " +
                    $"Longitude: {GeoMath.MIN_LON} - {GeoMath.MAX_LON}");

            var boundaryCoordinates = GeoMath.BoundingCoordinates(location, distanceKm, EARTH_RADIUS_KM);
            bool meridian180WithinDistance =
                boundaryCoordinates.MinCoordinates.Longitude > boundaryCoordinates.MaxCoordinates.Longitude;

            IEnumerable<Location> locations = null;

            if (string.IsNullOrEmpty(featureCodeId))
            {
                locations = await _locationsRepository.GetWithinBoundary(location,
                    meridian180WithinDistance, boundaryCoordinates, "id", false);
            }
            else
            {
                locations = await _locationsRepository.GetWithinBoundaryByFeature(featureCodeId, location,
                    meridian180WithinDistance, boundaryCoordinates, "id", false);
            }

            List<LocationResponse> locationList = FilterLocationsByDistance(location,
                locations, distanceKm);

            return locationList.Count;
        }

        private List<LocationResponse> FilterLocationsByDistance(GeoCoordinates location,
            IEnumerable<Location> locationsToFilter, double distanceKm)
        {
            List<LocationResponse> locationList = new List<LocationResponse>();

            foreach (var loc in locationsToFilter)
            {
                GeoCoordinates geoCoordinates = new GeoCoordinates
                {
                    Latitude = loc.Latitude,
                    Longitude = loc.Longitude
                };

                double actualDistanceKm = GeoMath.SphericalLawOfCosines(geoCoordinates, location);
                //double actualDistanceKm = GeoMath.Vicentry(geoCoordinates, location);

                if (actualDistanceKm <= distanceKm)
                {
                    geoCoordinates.Latitude = Math.Round(geoCoordinates.Latitude, GEO_DECIMAL_PLACES);
                    geoCoordinates.Longitude = Math.Round(geoCoordinates.Longitude, GEO_DECIMAL_PLACES);
                    
                    actualDistanceKm = Math.Round(actualDistanceKm, DISTANCE_DECIMAL_PLACES);

                    locationList.Add(new LocationResponse
                    {
                        Id = loc.Id,
                        Name = loc.Name,
                        Coordinates = geoCoordinates,
                        DistanceKm = actualDistanceKm,
                        CountryCodeId = loc.CountryCode.Id,
                        FeatureCodeId = loc.FeatureCode.Id,
                    });
                }
            }

            return locationList;
        }

        public async Task<CountryCodeResponse> GetCountryCode(string id)
        {
            var countryCode = await _locationsRepository.GetCountryCode(id);

            if (countryCode == null)
                throw new NotFoundException("Country code not found");

            return GetCountryCodeResponse(countryCode);
        }

        public async Task<IEnumerable<CountryCodeResponse>> GetCountryCodes()
        {
            var countryCodes = await _locationsRepository.GetAllCountryCodes("name", false);

            return GetCountryCodeResponseList(countryCodes);
        }

        public async Task<FeatureCodeResponse> GetFeatureCode(string id)
        {
            var featureCode = await _locationsRepository.GetFeatureCode(id);

            if (featureCode == null)
                throw new NotFoundException("Feature code not found");

            return GetFeatureCodeResponse(featureCode);
        }

        public async Task<IEnumerable<FeatureCodeResponse>> GetFeatureCodes()
        {
            var featureCodes = await _locationsRepository.GetAllFeatureCodes("name", false);

            return GetFeatureCodeResponseList(featureCodes);
        }

        private (string, bool) GetSortOrderString(string sortOrder)
        {
            string sortOrderToUse = "name";
            bool descending = false;

            if (!string.IsNullOrEmpty(sortOrder))
            {
                switch (sortOrder)
                {
                    case "id":
                    case "+id":
                    case "-id":
                        sortOrderToUse = "id";
                        break;
                    case "name":
                    case "+name":
                    case "-name":
                        sortOrderToUse = "name";
                        break;
                    case "lat":
                    case "+lat":
                    case "-lat":
                        sortOrderToUse = "latitude";
                        break;
                    case "lon":
                    case "+lon":
                    case "-lon":
                        sortOrderToUse = "longitude";
                        break;
                    default:
                        throw new ArgumentException("Invalid sort column");
                }

                //If the sort order string starts with a '-' then we want to sort
                //descending, otherwise we sort ascending
                if (sortOrder[0] == '-')
                    descending = true;
                else
                    descending = false;
            }

            return (sortOrderToUse, descending);
        }

        private LocationResponse GetLocationResponse(Location location, double distanceKm)
        {
            double latitudeDegrees = location.Latitude;
            double longitudeDegrees = location.Longitude;

            //Round the latitude and longitude to 6 decimal places
            latitudeDegrees = Math.Round(latitudeDegrees, 6);
            longitudeDegrees = Math.Round(longitudeDegrees, 6);

            return new LocationResponse
            {
                Id = location.Id,
                Name = location.Name,
                Coordinates = new GeoCoordinates { Latitude = latitudeDegrees, Longitude = longitudeDegrees },
                DistanceKm = distanceKm,
                CountryCodeId = location.CountryCode.Id,
                FeatureCodeId = location.FeatureCode.Id,
                HRef = null //To be filled in by the controller if required
            };
        }

        private IEnumerable<LocationResponse> GetLocationResponseList(IEnumerable<Location> locations)
        {
            List<LocationResponse> locationList = new List<LocationResponse>();
            foreach(var location in locations)
            {
                var locationResponse = GetLocationResponse(location, 0.0);
                locationList.Add(locationResponse);
            }

            return locationList;
        }

        private CountryCodeResponse GetCountryCodeResponse(CountryCode countryCode)
        {
            return new CountryCodeResponse
            {
                Id = countryCode.Id,
                Name = countryCode.Name,
                Alpha3Code = countryCode.Alpha3Code,
                NumericCode = countryCode.NumericCode
            };
        }

        private IEnumerable<CountryCodeResponse> GetCountryCodeResponseList(IEnumerable<CountryCode> countryCodes)
        {
            List<CountryCodeResponse> countryCodeList = new List<CountryCodeResponse>();
            foreach(var countryCode in countryCodes)
            {
                var countryCodeResponse = GetCountryCodeResponse(countryCode);
                countryCodeList.Add(countryCodeResponse);
            }

            return countryCodeList;
        }

        private FeatureCodeResponse GetFeatureCodeResponse(FeatureCode featureCode)
        {
            return new FeatureCodeResponse
            {
                Id = featureCode.Id,
                Name = featureCode.Name
            };
        }

        private IEnumerable<FeatureCodeResponse> GetFeatureCodeResponseList(IEnumerable<FeatureCode> featureCodes)
        {
            List<FeatureCodeResponse> featureCodeList = new List<FeatureCodeResponse>();
            foreach(var featureCode in featureCodes)
            {
                var featureCodeResponse = GetFeatureCodeResponse(featureCode);
                featureCodeList.Add(featureCodeResponse);
            }

            return featureCodeList;
        }
    }
}
