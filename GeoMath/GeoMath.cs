﻿using System;

namespace GeoMathLib
{
    public class BoundaryCoordinates
    {
        public GeoCoordinates MinCoordinates { get; set; }
        public GeoCoordinates MaxCoordinates { get; set; }
    }

    public struct GeoCoordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class GeoMath
    {
        //Minimum and maxium values of the latitude and longitude
        public static readonly double MIN_LAT = -90.0;
        public static readonly double MAX_LAT = 90.0;
        public static readonly double MIN_LON = -180.0;
        public static readonly double MAX_LON = 180.0;
        public static readonly double DEGREES_TO_RADIANS = (Math.PI / 180.0);
        public static readonly double RADIANS_TO_DEGREES = (180.0 / Math.PI);

        private static readonly double MIN_LAT_RADIANS = DegreesToRadians(MIN_LAT);
        private static readonly double MAX_LAT_RADIANS = DegreesToRadians(MAX_LAT);
        private static readonly double MIN_LON_RADIANS = DegreesToRadians(MIN_LON);
        private static readonly double MAX_LON_RADIANS = DegreesToRadians(MAX_LON);

        //Spherical approximation of the earth's radius in Km. This is used by
        //the Spherical Law of Cosines and Haversine calculations. The Vicentry
        //calculation doesn't need the approximation because it incorporates the
        //radius at the poles and the radius at the equator to give a more precise
        //answer (though it is much more computationally expensive)
        private static readonly double EARTH_RADIUS_KM = 6371.01;

        public static double DegreesToRadians(double degrees)
        {
            return DEGREES_TO_RADIANS * degrees;
        }

        public static GeoCoordinates DegreesToRadians(GeoCoordinates geoCoordinatesDegrees)
        {
            return new GeoCoordinates
            {
                Latitude = DegreesToRadians(geoCoordinatesDegrees.Latitude),
                Longitude = DegreesToRadians(geoCoordinatesDegrees.Longitude)
            };
        }

        public static double RadiansToDegrees(double radians)
        {
            return RADIANS_TO_DEGREES * radians;
        }

        public static GeoCoordinates RadiansToDegrees(GeoCoordinates geoCoordinatesRadians)
        {
            return new GeoCoordinates
            {
                Latitude = RadiansToDegrees(geoCoordinatesRadians.Latitude),
                Longitude = RadiansToDegrees(geoCoordinatesRadians.Longitude)
            };
        }

        public static double KmToMiles(double km)
        {
            return km * 0.62137119;
        }

        public static bool ValidCoordinates(double latitude, double longitude)
        {
            return (latitude >= MIN_LAT && latitude <= MAX_LAT) && (longitude >= MIN_LON && longitude <= MAX_LON);
        }

        public static bool ValidCoordinates(GeoCoordinates geoCoordinates)
        {
            return ValidCoordinates(geoCoordinates.Latitude, geoCoordinates.Longitude);
        }

        //https://en.wikipedia.org/wiki/Great-circle_distance
        public static double SphericalLawOfCosines(GeoCoordinates coordinates1, GeoCoordinates coordinates2)
        {
            double latitude1Radians = DegreesToRadians(coordinates1.Latitude);
            double longitude1Radians = DegreesToRadians(coordinates1.Longitude);
            double latitude2Radians = DegreesToRadians(coordinates2.Latitude);
            double longitude2Radians = DegreesToRadians(coordinates2.Longitude);

            //double latitudeDiff = latitude2Radians - latitude1Radians;
            double longitudeDiff = longitude2Radians - longitude1Radians;

            double angle = Math.Acos(Math.Sin(latitude1Radians) * Math.Sin(latitude2Radians) +
                Math.Cos(latitude1Radians) * Math.Cos(latitude2Radians) * Math.Cos(longitudeDiff));

            return EARTH_RADIUS_KM * angle;
        }

        public static double SphericalLawOfCosinesRadians(GeoCoordinates coordinates1, GeoCoordinates coordinates2)
        {
            //double latitudeDiff = latitude2Radians - latitude1Radians;
            double longitudeDiff = coordinates2.Longitude - coordinates1.Longitude;

            double angle = Math.Acos(Math.Sin(coordinates1.Latitude) * Math.Sin(coordinates2.Latitude) +
                Math.Cos(coordinates1.Latitude) * Math.Cos(coordinates2.Latitude) * Math.Cos(longitudeDiff));

            return EARTH_RADIUS_KM * angle;
        }

        //https://en.wikipedia.org/wiki/Haversine_formula
        public static double Haversine(GeoCoordinates coordinates1, GeoCoordinates coordinates2)
        {
            double latitude1Radians = DegreesToRadians(coordinates1.Latitude);
            double longitude1Radians = DegreesToRadians(coordinates1.Longitude);
            double latitude2Radians = DegreesToRadians(coordinates2.Latitude);
            double longitude2Radians = DegreesToRadians(coordinates2.Longitude);

            double latitudeDiff = latitude2Radians - latitude1Radians;
            double longitudeDiff = longitude2Radians - longitude1Radians;

            double tmp =
                Math.Pow(Math.Sin(latitudeDiff / 2), 2) + Math.Cos(latitude1Radians) * Math.Cos(latitude2Radians) *
                Math.Pow(Math.Sin(longitudeDiff / 2), 2);

            //The value of tmp could go above 1.0 because of floating point rounding errors. Clamp
            //the value to 1.0.
            //See https://en.wikipedia.org/wiki/Haversine_formula#Formulation
            if (tmp > 1.0)
                tmp = 1.0;

            double distance = 2 * EARTH_RADIUS_KM * Math.Asin(Math.Sqrt(tmp));

            return distance;
        }

        public static double HaversineRadians(GeoCoordinates coordinates1, GeoCoordinates coordinates2)
        {
            double latitudeDiff = coordinates2.Latitude - coordinates1.Latitude;
            double longitudeDiff = coordinates2.Longitude - coordinates1.Longitude;

            double tmp =
                Math.Pow(Math.Sin(latitudeDiff / 2), 2) + Math.Cos(coordinates1.Latitude) * Math.Cos(coordinates2.Latitude) *
                Math.Pow(Math.Sin(longitudeDiff / 2), 2);

            //The value of tmp could go above 1.0 because of floating point rounding errors. Clamp
            //the value to 1.0.
            //See https://en.wikipedia.org/wiki/Haversine_formula#Formulation
            if (tmp > 1.0)
                tmp = 1.0;

            double distance = 2 * EARTH_RADIUS_KM * Math.Asin(Math.Sqrt(tmp));

            return distance;
        }

        //https://en.wikipedia.org/wiki/Vincenty%27s_formulae
        //Ported from https://www.r-bloggers.com/great-circle-distance-calculations-in-r/
        //Also see https://www.movable-type.co.uk/scripts/latlong-vincenty.html
        public static double Vicentry(GeoCoordinates coordinates1, GeoCoordinates coordinates2, int iterations = 100)
        {
            double latitude1Radians = DegreesToRadians(coordinates1.Latitude);
            double longitude1Radians = DegreesToRadians(coordinates1.Longitude);
            double latitude2Radians = DegreesToRadians(coordinates2.Latitude);
            double longitude2Radians = DegreesToRadians(coordinates2.Longitude);

            double longitudeDiff = longitude2Radians - longitude1Radians;

            //WGS-84 ellipsoid parameters
            const double a = 6378137;              //length of major axis of the ellipsoid (radius at equator) in metres
            const double b = 6356752.314245;       //length of minor axis of the ellipsoid (radius at the poles) in metres
            const double f = 1 / 298.257223563;    //flattening of the ellipsoid

            double u1 = Math.Atan((1 - f) * Math.Tan(latitude1Radians)); //reduced latitude
            double u2 = Math.Atan((1 - f) * Math.Tan(latitude2Radians)); //reduced latitude

            double sinU1 = Math.Sin(u1);
            double cosU1 = Math.Cos(u1);
            double sinU2 = Math.Sin(u2);
            double cosU2 = Math.Cos(u2);

            double cosSqAlpha = 0.0;
            double sinSigma = 0.0;
            double cosSigma = 0.0;
            double cos2SigmaM = 0.0;
            double sigma = 0.0;

            double lambda = longitudeDiff;
            double lambdaPrev = 0;

            while (Math.Abs(lambda - lambdaPrev) > 1e-12 && iterations > 0)
            {
                double sinLambda = Math.Sin(lambda);
                double cosLambda = Math.Cos(lambda);

                sinSigma = Math.Sqrt(
                    (cosU2 * sinLambda) * (cosU2 * sinLambda) +
                    (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                );

                if (sinSigma == 0)
                    return 0.0;     //Co-incident points

                cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
                sigma = Math.Atan2(sinSigma, cosSigma);
                double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
                cosSqAlpha = 1 - sinAlpha * sinAlpha;
                cos2SigmaM = cosSigma - ((2 * sinU1 * sinU2) / cosSqAlpha);

                if (Double.IsNaN(cos2SigmaM))
                    cos2SigmaM = 0.0;   //Equatorial line : cosSqAlpha = 0

                double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
                lambdaPrev = lambda;
                lambda = longitudeDiff + (1 - C) * f * sinAlpha *
                    (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

                --iterations;
            }

            //For now throw an exception. Might want to do something different later
            if (iterations == 0)
                throw new Exception("formula failed to converge");     //formula failed to converge

            double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
            double A = 1 + (uSq / 16384) * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
            double B = (uSq / 1024) * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

            double deltaSigma =
                B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * Math.Pow(cos2SigmaM, 2)) -
                B / 6 * cos2SigmaM * (-3 + 4 * Math.Pow(sinSigma, 2)) * (-3 + 4 * Math.Pow(cos2SigmaM, 2))));

            double distance = b * A * (sigma - deltaSigma) / 1000;       //Distance in Km

            return distance;
        }

        public static double VicentryRadians(GeoCoordinates coordinates1, GeoCoordinates coordinates2, int iterations = 100)
        {
            double longitudeDiff = coordinates2.Longitude - coordinates1.Longitude;

            //WGS-84 ellipsoid parameters
            const double a = 6378137;              //length of major axis of the ellipsoid (radius at equator) in metres
            const double b = 6356752.314245;       //length of minor axis of the ellipsoid (radius at the poles) in metres
            const double f = 1 / 298.257223563;    //flattening of the ellipsoid

            double u1 = Math.Atan((1 - f) * Math.Tan(coordinates1.Latitude)); //reduced latitude
            double u2 = Math.Atan((1 - f) * Math.Tan(coordinates2.Latitude)); //reduced latitude

            double sinU1 = Math.Sin(u1);
            double cosU1 = Math.Cos(u1);
            double sinU2 = Math.Sin(u2);
            double cosU2 = Math.Cos(u2);

            double cosSqAlpha = 0.0;
            double sinSigma = 0.0;
            double cosSigma = 0.0;
            double cos2SigmaM = 0.0;
            double sigma = 0.0;

            double lambda = longitudeDiff;
            double lambdaPrev = 0;

            while (Math.Abs(lambda - lambdaPrev) > 1e-12 && iterations > 0)
            {
                double sinLambda = Math.Sin(lambda);
                double cosLambda = Math.Cos(lambda);

                sinSigma = Math.Sqrt(
                    (cosU2 * sinLambda) * (cosU2 * sinLambda) +
                    (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                );

                if (sinSigma == 0)
                    return 0.0;     //Co-incident points

                cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
                sigma = Math.Atan2(sinSigma, cosSigma);
                double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
                cosSqAlpha = 1 - sinAlpha * sinAlpha;
                cos2SigmaM = cosSigma - ((2 * sinU1 * sinU2) / cosSqAlpha);

                if (Double.IsNaN(cos2SigmaM))
                    cos2SigmaM = 0.0;   //Equatorial line : cosSqAlpha = 0

                double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
                lambdaPrev = lambda;
                lambda = longitudeDiff + (1 - C) * f * sinAlpha *
                    (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

                --iterations;
            }

            //For now throw an exception. Might want to do something different later
            if (iterations == 0)
                throw new Exception("formula failed to converge");     //formula failed to converge

            double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
            double A = 1 + (uSq / 16384) * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
            double B = (uSq / 1024) * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

            double deltaSigma =
                B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * Math.Pow(cos2SigmaM, 2)) -
                B / 6 * cos2SigmaM * (-3 + 4 * Math.Pow(sinSigma, 2)) * (-3 + 4 * Math.Pow(cos2SigmaM, 2))));

            double distance = b * A * (sigma - deltaSigma) / 1000;       //Distance in Km

            return distance;
        }

        //Ported from: http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
        public static BoundaryCoordinates BoundingCoordinates(GeoCoordinates location, double distance, double radius)
        {
            if (radius < 0.0 || distance < 0.0)
                throw new ArgumentException("Invalid distance or radius");

            double locationLatitude = DegreesToRadians(location.Latitude);
            double locationLongitude = DegreesToRadians(location.Longitude);

            // angular distance in radians on a great circle
            double radDist = distance / radius;

            double minLat = locationLatitude - radDist;
            double maxLat = locationLatitude + radDist;

            double minLon = 0.0;
            double maxLon = 0.0;
            if (minLat > MIN_LAT_RADIANS && maxLat < MAX_LAT_RADIANS)
            {
                double deltaLon = Math.Asin(Math.Sin(radDist) / Math.Cos(locationLatitude));
                minLon = locationLongitude - deltaLon;
                if (minLon < MIN_LON_RADIANS)
                    minLon += 2d * Math.PI;
                maxLon = locationLongitude + deltaLon;
                if (maxLon > MAX_LON_RADIANS)
                    maxLon -= 2d * Math.PI;
            }
            else
            {
                // a pole is within the distance
                minLat = Math.Max(minLat, MIN_LAT_RADIANS);
                maxLat = Math.Min(maxLat, MAX_LAT_RADIANS);
                minLon = MIN_LON_RADIANS;
                maxLon = MAX_LON_RADIANS;
            }

            double minLatitudeInDegrees = RadiansToDegrees(minLat);
            double minLongitudeInDegrees = RadiansToDegrees(minLon);
            double maxLatitudeInDegrees = RadiansToDegrees(maxLat);
            double maxLongitudeInDegrees = RadiansToDegrees(maxLon);

            return new BoundaryCoordinates()
            {
                MinCoordinates = new GeoCoordinates() { Latitude = minLatitudeInDegrees, Longitude = minLongitudeInDegrees },
                MaxCoordinates = new GeoCoordinates() { Latitude = maxLatitudeInDegrees, Longitude = maxLongitudeInDegrees }
            };
        }

        public static BoundaryCoordinates BoundingCoordinatesRadians(GeoCoordinates location, double distance, double radius)
        {
            if (radius < 0.0 || distance < 0.0)
                throw new ArgumentException("Invalid distance or radius");

            // angular distance in radians on a great circle
            double radDist = distance / radius;

            double minLat = location.Latitude - radDist;
            double maxLat = location.Latitude + radDist;

            double minLon = 0.0;
            double maxLon = 0.0;
            if (minLat > MIN_LAT_RADIANS && maxLat < MAX_LAT_RADIANS)
            {
                double deltaLon = Math.Asin(Math.Sin(radDist) / Math.Cos(location.Latitude));
                minLon = location.Longitude - deltaLon;
                if (minLon < MIN_LON_RADIANS)
                    minLon += 2d * Math.PI;
                maxLon = location.Longitude + deltaLon;
                if (maxLon > MAX_LON_RADIANS)
                    maxLon -= 2d * Math.PI;
            }
            else
            {
                // a pole is within the distance
                minLat = Math.Max(minLat, MIN_LAT_RADIANS);
                maxLat = Math.Min(maxLat, MAX_LAT_RADIANS);
                minLon = MIN_LON_RADIANS;
                maxLon = MAX_LON_RADIANS;
            }

            //We don't convert the boundary coordinates to degrees, we just return
            //the boundary coordinates as radians
            return new BoundaryCoordinates()
            {
                MinCoordinates = new GeoCoordinates() { Latitude = minLat, Longitude = minLon },
                MaxCoordinates = new GeoCoordinates() { Latitude = maxLat, Longitude = maxLon }
            };
        }
    }
}
